package com.termpaper.techdrawflow.j13termpapertdf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class J13TermPaperTdfApplication {

    public static void main(String[] args) {
        SpringApplication.run(J13TermPaperTdfApplication.class, args);
    }

}
