package com.termpaper.techdrawflow.j13termpapertdf.project.mapper;

import com.termpaper.techdrawflow.j13termpapertdf.project.dto.DepartmentDTO;
import com.termpaper.techdrawflow.j13termpapertdf.project.model.Department;
import com.termpaper.techdrawflow.j13termpapertdf.project.model.GenericModel;
import com.termpaper.techdrawflow.j13termpapertdf.project.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class DepartmentMapper
        extends GenericMapper<Department, DepartmentDTO>  {
    private final UserRepository userRepository;
    public DepartmentMapper(ModelMapper modelMapper,
                            UserRepository userRepository) {
        super(modelMapper, Department.class, DepartmentDTO.class);
        this.userRepository = userRepository;
    }



    @Override
    protected void setupMapper() {
        modelMapper.createTypeMap(Department.class, DepartmentDTO.class)
                .addMappings(m -> m.skip(DepartmentDTO::setUsersIds)).setPostConverter(toDTOConverter());
        modelMapper.createTypeMap(DepartmentDTO.class, Department.class)
                .addMappings(m -> m.skip(Department::setUsers)).setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(DepartmentDTO source, Department destination) {
        if (!Objects.isNull(source.getUsersIds())) {
            destination.setUsers(new HashSet<>(userRepository.findAllById(source.getUsersIds())));
        }
        else {
            destination.setUsers(Collections.emptySet());
        }
    }

    @Override
    protected void mapSpecificFields(Department source, DepartmentDTO destination) {
        destination.setUsersIds(getIds(source));
    }

    @Override
    protected Set<Long> getIds(Department entity) {
        return Objects.isNull(entity) || Objects.isNull(entity.getId())
                ? null
                : entity.getUsers().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toSet());
    }

}
