package com.termpaper.techdrawflow.j13termpapertdf.project.repository;

import com.termpaper.techdrawflow.j13termpapertdf.project.model.Document;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;

@Repository
public interface DocumentRepository
        extends GenericRepository<Document>  {



//    @Query(nativeQuery = true,
//            value = """
//                 select d.*
//                 from documents d
//                 where d.document_number ilike '%' || coalesce(:documentNumber, '%') || '%'
//                 and d.document_title ilike '%' || coalesce(:documentTitle, '%') || '%'
//                 and d.document_type '%' || coalesce(:documentType, '%') || '%'
//                  """)
//    Page<Document> searchDocuments(String documentNumber,
//                                   String documentTitle,
//                                   String documentType,
//                                   Pageable pageable);
//}


    @Query(nativeQuery = true,
            value = """
                 select distinct d.*
                 from documents d
                right join document_versions dv on d.id = dv.document_id
                 where d.document_number ilike '%' || coalesce(:documentNumber, '%') || '%'
                 and d.document_title ilike '%' || coalesce(:documentTitle, '%') || '%'
                and cast(d.document_type  as char) like coalesce(:documentType,'%')
                  """)
    Page<Document> searchDocuments(@Param(value = "documentNumber") String documentNumber,
                                   @Param(value = "documentTitle") String documentTitle,
                                   @Param(value = "documentType") String documentType,
                                   Pageable pageable);


}