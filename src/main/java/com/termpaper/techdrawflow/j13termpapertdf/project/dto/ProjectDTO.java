package com.termpaper.techdrawflow.j13termpapertdf.project.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProjectDTO extends GenericDTO{



    private String projectName;

    private String  description;

    private Set<Long> documentsIds;


}