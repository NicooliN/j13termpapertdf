package com.termpaper.techdrawflow.j13termpapertdf.project.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Entity
@Table(name = "projects")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SequenceGenerator(name = "default_generator", sequenceName = "projects_seq", allocationSize = 1)
public class Project extends GenericModel{

    @OneToMany(mappedBy = "project", cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    private Set<Document> documents;

    @Column(name = "project_name")
    private String projectName;

    @Column(name = "description")
    private String  description;
}