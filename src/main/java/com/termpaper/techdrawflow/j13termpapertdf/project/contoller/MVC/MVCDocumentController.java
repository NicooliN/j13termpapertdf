package com.termpaper.techdrawflow.j13termpapertdf.project.contoller.MVC;



import com.termpaper.techdrawflow.j13termpapertdf.project.dto.DocumentDTO;
import com.termpaper.techdrawflow.j13termpapertdf.project.dto.DocumentSearchDTO;
import com.termpaper.techdrawflow.j13termpapertdf.project.dto.DocumentWithVersionsDTO;
import com.termpaper.techdrawflow.j13termpapertdf.project.exception.MyDeleteException;
import com.termpaper.techdrawflow.j13termpapertdf.project.service.DocumentService;
import com.termpaper.techdrawflow.j13termpapertdf.project.service.ProjectService;
import com.termpaper.techdrawflow.j13termpapertdf.project.service.UserService;
import io.swagger.v3.oas.annotations.Hidden;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;


@Controller
@Hidden
@Slf4j
@RequestMapping("/documents")
public class MVCDocumentController {

    private final DocumentService documentService;
private final ProjectService projectService;
    private final UserService userService;
    public MVCDocumentController(DocumentService documentService,
                                 UserService userService,
                                 ProjectService projectService) {
        this.documentService = documentService;
        this.userService = userService;
        this.projectService = projectService;
    }

//    @GetMapping("/list")
//    public String listAllDocuments(@RequestParam(value = "page", defaultValue = "1") int page,
//                               @RequestParam(value = "size", defaultValue = "10") int pageSize,
//                               @ModelAttribute(value = "exception") String exception,
//                               Model model) {
//        log.info("Hello from log");
//        PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Direction.ASC, "document_number"));
//        Page<DocumentWithVersionsDTO> result = documentService.getAllDocumentsWithVersions(pageRequest);
//        model.addAttribute("documents", result);
//        model.addAttribute("exception", exception);
//        return "documents/viewAllDocuments";
//    }

    @GetMapping("/{id}")
    public String  getOne(@PathVariable Long id,
                             Model model) {
        model.addAttribute("document", documentService.getDocument(id));
        return "documents/viewDocument";
    }

//    @GetMapping("/documents/{id}")
//    public String getDoc(@PathVariable Long id,
//                         Model model) {
//        model.addAttribute("document", documentService.getDocument(id));
//        return "documents/viewAllProject";
//    }

    @GetMapping("/add/{projectId}")
    public String create(@PathVariable Long projectId,
                              Model model) {
        model.addAttribute("projectId", projectId);
        model.addAttribute("project" , projectService.getOne(projectId).getProjectName());
        return "documents/addDocument";}

    @PostMapping("/add")
    public String create(@ModelAttribute("documentForm") DocumentDTO documentDTO) {
        documentService.create(documentDTO);
        log.info(documentDTO.toString());
        return "redirect:/projects/list";
    }

    @GetMapping("/update/{id}")
    public String update(@PathVariable Long id,
                         Model model) {
        model.addAttribute("document", documentService.getOne(id));
        return "documents/updateDocument";
    }

    @PostMapping("/update")
    public String update(@ModelAttribute("bookForm") DocumentDTO documentDTO,
                         @RequestParam MultipartFile file) {
            documentService.update(documentDTO);
        return "redirect:/documents";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable Long id) throws MyDeleteException {
            documentService.delete(id);
        return "redirect:/projects/list";
    }

    @GetMapping("/restore/{id}")
    public String restore(@PathVariable Long id) {
        documentService.restore(id);
        return "redirect:/projects/list";
    }

//    @ExceptionHandler(MyDeleteException.class)
//    public RedirectView handleError(HttpServletRequest req,
//                                    Exception ex,
//                                    RedirectAttributes redirectAttributes) {
//        log.error("Запрос: " + req.getRequestURL() + " вызвал ошибку " + ex.getMessage());
//        redirectAttributes.addFlashAttribute("exception", ex.getMessage());
//        return new RedirectView("/documents", true);
//    }

}
