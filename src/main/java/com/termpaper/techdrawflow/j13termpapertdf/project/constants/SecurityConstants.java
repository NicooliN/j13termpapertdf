package com.termpaper.techdrawflow.j13termpapertdf.project.constants;

import java.util.List;

public interface SecurityConstants {
    
    List<String> RESOURCES_WHITE_LIST = List.of("/resources/**",
                                                "/js/**",
                                                "/css/**",
                                                "/",
                                                // -- Swagger UI v3 (OpenAPI)
                                                "/swagger-ui/**",
                                                "/webjars/bootstrap/5.0.2/**",
                                                "/v3/api-docs/**");
    
    List<String> PROJECT_WHITE_LIST = List.of("/projects",
                                            "/projects/search",
                                            "/projects/{id}",
                                     "/projects/{id}/search");

    List<String> PROJECT_PERMISSION_LIST = List.of("/projects/add",
                                                 "/projects/update",
                                                 "/projects/delete"
                                                 );

    List<String> DEPARTMENT_PERMISSION_LIST = List.of("/departments/add",
            "/departments/update"
    );


    List<String> USERS_WHITE_LIST = List.of("/login",
                                            "/users/registration",
                                            "/users/remember-password",
                                            "/users/change-password");


    List<String> DOCUMENT_WHITE_LIST  = List.of("/documents/add",
            "/documents/list",
            "/documents/update",
            "/documents/delete",
            "/documents/add/{projectId}",
            "/documents/{id}");

    List<String> DOCUMENT_VERSION_WHITE_LIST  = List.of("/documentsVersions/add",
            "/documentsVersions/list",
            "/documentsVersions/update",
            "/documentsVersions/delete",
            "/documentsVersions/{id}",
            "/documentsVersions/add/{documentId}",
            "/documentsVersions/download/{documentsVersionsId}");

//    List<String> USERS_REST_WHITE_LIST = List.of("/users/auth",
//            "/users/reg",
//
//            "/users/user");

}
