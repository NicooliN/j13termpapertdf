package com.termpaper.techdrawflow.j13termpapertdf.project.mapper;


import com.termpaper.techdrawflow.j13termpapertdf.project.dto.GenericDTO;
//import com.termpaper.techdrawflow.j13termpapertdf.project.dto.ProjectWithDocumentsDTO;
import com.termpaper.techdrawflow.j13termpapertdf.project.model.GenericModel;
import com.termpaper.techdrawflow.j13termpapertdf.project.model.Project;
import jakarta.annotation.PostConstruct;

import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;

import java.util.List;
import java.util.Objects;
import java.util.Set;

public abstract class GenericMapper <E extends GenericModel, D extends GenericDTO>
    implements Mapper<E, D> {

    protected final ModelMapper modelMapper;

    private final Class<E> entityClass;

    private final Class<D> dtoClass;

    public GenericMapper(ModelMapper modelMapper,
                         Class<E> entityClass,
                         Class<D> dtoClass) {
        this.modelMapper = modelMapper;
        this.entityClass = entityClass;
        this.dtoClass = dtoClass;
    }
    @Override
    public E toEntity(D dto) {
        return Objects.isNull(dto)
                ? null
                : modelMapper.map(dto, entityClass);
    }

    public List<E> toEntities(List<D> dtos) {
        return dtos.stream().map(this::toEntity).toList();
    }

    @Override
    public D toDTO (E entity) {
        return Objects.isNull(entity)
                ? null
                : modelMapper.map(entity, dtoClass);
    }
@Override
    public List<D> toDTOs(List<E> entities) {
        return entities.stream().map(this::toDTO).toList();
    }

    Converter<E, D> toDTOConverter (){
        return context -> {
            E source = context.getSource();
            D destination = context.getDestination();
            mapSpecificFields(source, destination);
            return context.getDestination();
        };
    }
    Converter<D, E> toEntityConverter (){

        return context -> {
            D source = context.getSource();
            E destination = context.getDestination();
            mapSpecificFields(source, destination);
            return context.getDestination();
        };
    }

    protected abstract void mapSpecificFields(E source, D destination);

    protected abstract void mapSpecificFields(D source, E destination);

    protected abstract Set<Long> getIds(E entity);

    @PostConstruct
    protected abstract void setupMapper();
}
