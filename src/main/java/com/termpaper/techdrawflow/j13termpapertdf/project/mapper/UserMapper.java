package com.termpaper.techdrawflow.j13termpapertdf.project.mapper;


import com.termpaper.techdrawflow.j13termpapertdf.project.dto.UserDTO;
import com.termpaper.techdrawflow.j13termpapertdf.project.model.User;
import com.termpaper.techdrawflow.j13termpapertdf.project.repository.DepartmentRepository;
import com.termpaper.techdrawflow.j13termpapertdf.project.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import org.webjars.NotFoundException;

import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class UserMapper
        extends GenericMapper<User, UserDTO> {

    private final UserRepository userRepository;
    private final DepartmentRepository departmentRepository;

    //    private final
//
    protected UserMapper(ModelMapper modelMapper,
                         UserRepository userRepository,
                         DepartmentRepository departmentRepository) {
        super(modelMapper, User.class, UserDTO.class);

        this.userRepository = userRepository;
        this.departmentRepository = departmentRepository;
    }


    @Override
    protected void setupMapper() {
        modelMapper.createTypeMap(User.class, UserDTO.class)
                .addMappings(m -> m.skip(UserDTO::setDepartmentId)).setPostConverter(toDTOConverter());
        modelMapper.createTypeMap(UserDTO.class, User.class)
                .addMappings(m -> m.skip(User::setDepartment)).setPostConverter(toEntityConverter());
   }
    @Override
    protected void mapSpecificFields(User source, UserDTO destination){
        destination.setDepartmentId(source.getDepartment().getId());
    }

    @Override
    protected void mapSpecificFields(UserDTO source, User destination){
        destination.setDepartment(departmentRepository.findById(source.getDepartmentId()).orElseThrow(() -> new NotFoundException("data user not found by this id")));
//
//            destination.(new HashSet<>(.findAllById(source.getUserOrders())));
//        }
//        else {
//            destination.(Collections.emptySet());
//        }
    }
        @Override
        protected Set<Long> getIds(User entity){
           throw new UnsupportedOperationException("Метод недоступен");
    }

}
