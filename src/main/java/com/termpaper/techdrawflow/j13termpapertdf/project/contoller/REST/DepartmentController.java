package com.termpaper.techdrawflow.j13termpapertdf.project.contoller.REST;

import com.termpaper.techdrawflow.j13termpapertdf.project.dto.DepartmentDTO;
import com.termpaper.techdrawflow.j13termpapertdf.project.model.Department;
import com.termpaper.techdrawflow.j13termpapertdf.project.service.DepartmentService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/departments")
@Tag(name = "Подразделения", description = "контроллер для работы с подразделениями")
public class DepartmentController extends GenericController<Department, DepartmentDTO> {


    public DepartmentController(DepartmentService departmentService){
        super(departmentService);
    }


}
