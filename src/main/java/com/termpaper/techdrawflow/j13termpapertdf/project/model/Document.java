package com.termpaper.techdrawflow.j13termpapertdf.project.model;


import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import jakarta.persistence.*;
import java.time.LocalDate;
import java.util.Set;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import jakarta.persistence.SequenceGenerator;
import lombok.*;

@Entity
@Table(name = "documents")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@SequenceGenerator(name = "default_generator", sequenceName = "documents_seq",allocationSize = 1)
public class  Document extends GenericModel {

    @Column(name = "document_number", nullable = false)
    private String documentNumber;

    @Column(name = "document_title", nullable = false)
    private String documentTitle;

     @Column(name = "document_type", nullable = false)
     @Enumerated
     private DocumentType documentType;

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "document_owner_id", foreignKey = @ForeignKey(name = "FK_DOCUMENTS_USER"))
    private User user;


    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "project_id", foreignKey = @ForeignKey(name = "FK_DOCUMENTS_PROJECT"))
    private Project project;

    @OneToMany(mappedBy = "document", cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @ToString.Exclude
    private Set<DocumentVersion> documentVersions;


}
