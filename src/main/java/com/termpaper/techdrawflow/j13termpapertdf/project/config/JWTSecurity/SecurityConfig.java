//package com.termpaper.techdrawflow.j13termpapertdf.project.config.JWTSecurity;
//
//import com.termpaper.techdrawflow.j13termpapertdf.project.config.JWTSecurity.JWTTokenFilter;
//import com.termpaper.techdrawflow.j13termpapertdf.project.service.userdetails.CustomUserDetailsService;
//import jakarta.servlet.http.HttpServletResponse;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.security.authentication.AuthenticationManager;
//import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
//import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
//import org.springframework.security.config.http.SessionCreationPolicy;
//import org.springframework.security.web.SecurityFilterChain;
//import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
//import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
//import org.springframework.web.cors.CorsConfiguration;
//import org.springframework.web.cors.CorsConfigurationSource;
//import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
//import org.springframework.web.servlet.config.annotation.CorsRegistry;
//import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
//
//
//import java.util.Arrays;
//
//import static com.termpaper.techdrawflow.j13termpapertdf.project.constants.SecurityConstants.RESOURCES_WHITE_LIST;
//import static com.termpaper.techdrawflow.j13termpapertdf.project.constants.SecurityConstants.USERS_REST_WHITE_LIST;
//import static com.termpaper.techdrawflow.j13termpapertdf.project.constants.UserRolesConstants.ADMIN;
//import static com.termpaper.techdrawflow.j13termpapertdf.project.constants.UserRolesConstants.MODERATOR;
//
//@Configuration
//@EnableWebSecurity
//@EnableMethodSecurity
//public class SecurityConfig implements WebMvcConfigurer
//    {
//
//    private final CustomUserDetailsService customUserDetailsService;
//    private final JWTTokenFilter jwtTokenFilter;
//
//    public SecurityConfig(CustomUserDetailsService customUserDetailsService,
//                          JWTTokenFilter jwtTokenFilter) {
//        this.customUserDetailsService = customUserDetailsService;
//        this.jwtTokenFilter = jwtTokenFilter;
//    }
//
//
////    @Bean
////    public HttpFirewall httpFirewall() {
////        StrictHttpFirewall firewall = new StrictHttpFirewall();
//////        firewall.setAllowUrlEncodedPercent(true);
//////        firewall.setAllowUrlEncodedSlash(true);
//////        firewall.setAllowSemicolon(true);
////        firewall.setAllowedHttpMethods(Arrays.asList("GET", "POST", "PUT", "DELETE"));
////        return firewall;
////    }
////
//
//    @Override
//    public void addCorsMappings(CorsRegistry registry) {
//        registry.addMapping("/**")
//                .allowedOrigins("http://localhost:8081")
//                .allowedMethods("*");
//    }
//
//    @Bean
//    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
//        return http
//              .cors(AbstractHttpConfigurer::disable)
//              .csrf(AbstractHttpConfigurer::disable)
//              //Настройка http запросов - кому куда можно/нельзя
//              .authorizeHttpRequests(auth -> auth
//                                           .requestMatchers(RESOURCES_WHITE_LIST.toArray(String[]::new)).permitAll()
//                                           .requestMatchers(USERS_REST_WHITE_LIST.toArray(String[]::new)).permitAll()
//                                           .requestMatchers("/users/admin").hasAnyRole(ADMIN)
//                              .             requestMatchers("/users/mod").hasAnyRole(MODERATOR)
//                                           .anyRequest().authenticated()
//                                    )
//              .exceptionHandling()
//              .authenticationEntryPoint((request, response, authException) -> {
//                  response.sendError(HttpServletResponse.SC_UNAUTHORIZED,
//                                     authException.getMessage());
//              })
//              .and()
//              .sessionManagement(
//                    session -> session.sessionCreationPolicy(SessionCreationPolicy.ALWAYS))
//              //JWT Token Filter VALID or NOT
//              .addFilterBefore(jwtTokenFilter, UsernamePasswordAuthenticationFilter.class)
//              .userDetailsService(customUserDetailsService)
//              .build();
//    }
//
//    @Bean
//    public AuthenticationManager authenticationManager(
//          AuthenticationConfiguration authenticationConfiguration) throws Exception {
//        return authenticationConfiguration.getAuthenticationManager();
//    }
//        @Bean
//        CorsConfigurationSource corsConfigurationSource() {
//            CorsConfiguration configuration = new CorsConfiguration();
//            configuration.setAllowedOrigins(Arrays.asList("http://localhost:8081"));
//            configuration.setAllowedMethods(Arrays.asList("GET","POST","PATCH", "PUT", "DELETE", "OPTIONS", "HEAD"));
//            configuration.setAllowCredentials(true);
//            configuration.setAllowedHeaders(Arrays.asList("Authorization", "Requestor-Type"));
//            configuration.setExposedHeaders(Arrays.asList("X-Get-Header"));
//            configuration.setMaxAge(3600L);
//            UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
//            source.registerCorsConfiguration("/**", configuration);
//            return source;
//        }
//
//}
//
//
