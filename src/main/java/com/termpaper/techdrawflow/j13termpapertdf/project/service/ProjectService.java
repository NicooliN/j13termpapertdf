package com.termpaper.techdrawflow.j13termpapertdf.project.service;

import com.termpaper.techdrawflow.j13termpapertdf.project.dto.ProjectDTO;
import com.termpaper.techdrawflow.j13termpapertdf.project.dto.ProjectWithDocumentsDTO;
import com.termpaper.techdrawflow.j13termpapertdf.project.exception.MyDeleteException;
import com.termpaper.techdrawflow.j13termpapertdf.project.mapper.ProjectMapper;
import com.termpaper.techdrawflow.j13termpapertdf.project.mapper.ProjectWithDocumentsMapper;
import com.termpaper.techdrawflow.j13termpapertdf.project.model.Document;
import com.termpaper.techdrawflow.j13termpapertdf.project.model.Project;
import com.termpaper.techdrawflow.j13termpapertdf.project.repository.ProjectRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;
import java.util.List;

@Service
@Slf4j
public class ProjectService
                extends GenericService<Project, ProjectDTO> {

    ProjectWithDocumentsMapper projectWithDocumentsMapper;
    private final ProjectRepository projectRepository;

    public ProjectService(ProjectRepository projectRepository,
                          ProjectMapper projectMapper,
                           ProjectWithDocumentsMapper projectWithDocumentsMapper) {
        super(projectRepository, projectMapper);
        this.projectRepository = projectRepository;
        this.projectWithDocumentsMapper = projectWithDocumentsMapper;
    }

    public List<ProjectDTO> findProject (ProjectDTO projectDTO) {
        List<Project> result = projectRepository.searchProjects(projectDTO.getProjectName()) ;
        return  mapper.toDTOs(result);
    }

    //Здесь сервис
    public ProjectWithDocumentsDTO getProject(Long id) {
        return projectWithDocumentsMapper.toDTO(mapper.toEntity(super.getOne(id)));
    }

    public void restore(Long objectId) {
        Project project = projectRepository.findById(objectId).orElseThrow(
                () -> new NotFoundException("Проект с заданным ID=" + objectId + " не существует"));
        unMarkAsDeleted(project);
        projectRepository.save(project);
    }

    @Override
    public void delete(Long id) throws MyDeleteException {
        Project project = projectRepository.findById(id).orElseThrow(
                () -> new NotFoundException("Проект с заданным ID=" + id + " не существует"));
        markAsDeleted(project);
        projectRepository.save(project);

    }
}
