package com.termpaper.techdrawflow.j13termpapertdf.project.repository;

import com.termpaper.techdrawflow.j13termpapertdf.project.model.Project;
import com.termpaper.techdrawflow.j13termpapertdf.project.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProjectRepository
        extends GenericRepository<Project> {



    @Query(nativeQuery = true,
            value = """
                 select p.*
                 from projects p
                 where p.project_name ilike '%' || coalesce(:projectName, '%') || '%'
                  """)
    List<Project> searchProjects(String projectName);

}


// value = """
//                 select p.*
//                 from project p
//                 where p.project_name ilike '%' || coalesce(:projectName, '%') || '%'
//                  """
