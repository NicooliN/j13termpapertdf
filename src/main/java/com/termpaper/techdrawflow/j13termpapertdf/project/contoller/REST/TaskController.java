package com.termpaper.techdrawflow.j13termpapertdf.project.contoller.REST;

import com.termpaper.techdrawflow.j13termpapertdf.project.dto.TaskDTO;
import com.termpaper.techdrawflow.j13termpapertdf.project.model.Task;
import com.termpaper.techdrawflow.j13termpapertdf.project.service.TaskService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/tasks")
@Tag(name = "Задание", description = "")
public class TaskController
                extends GenericController<Task, TaskDTO>{

    public TaskController(TaskService taskService) {
        super(taskService);
    }
}
