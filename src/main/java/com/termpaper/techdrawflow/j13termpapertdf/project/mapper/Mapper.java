package com.termpaper.techdrawflow.j13termpapertdf.project.mapper;

import com.termpaper.techdrawflow.j13termpapertdf.project.dto.GenericDTO;
import com.termpaper.techdrawflow.j13termpapertdf.project.model.GenericModel;

import java.util.List;

public interface Mapper <E extends GenericModel, D extends GenericDTO> {
    
    E toEntity(D dto);
    
    List<E> toEntities(List<D> dtos);
    
    D toDTO( E entity);
    List<D> toDTOs(List<E> entities);
    
}
