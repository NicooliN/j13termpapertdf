package com.termpaper.techdrawflow.j13termpapertdf.project.mapper;


import com.termpaper.techdrawflow.j13termpapertdf.project.dto.DocumentDTO;
import com.termpaper.techdrawflow.j13termpapertdf.project.dto.DocumentWithVersionsDTO;
import com.termpaper.techdrawflow.j13termpapertdf.project.model.Document;
import com.termpaper.techdrawflow.j13termpapertdf.project.model.GenericModel;
import com.termpaper.techdrawflow.j13termpapertdf.project.repository.DocumentVersionRepository;
import com.termpaper.techdrawflow.j13termpapertdf.project.repository.ProjectRepository;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import org.webjars.NotFoundException;

import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;


//мапер
@Component
public class DocumentWithVersionsMapper
      extends GenericMapper<Document, DocumentWithVersionsDTO> {



    private final DocumentVersionRepository documentVersionRepository;

    protected DocumentWithVersionsMapper(ModelMapper mapper,
                                         DocumentVersionRepository documentVersionRepository) {
        super(mapper, Document.class, DocumentWithVersionsDTO.class);

        this.documentVersionRepository = documentVersionRepository;

    }

    @Override
    @PostConstruct
    protected void setupMapper() {
        modelMapper.createTypeMap(Document.class, DocumentWithVersionsDTO.class)
              .addMappings(m -> m.skip(DocumentWithVersionsDTO::setDocumentVersionsIds)).setPostConverter(toDTOConverter());
        modelMapper.createTypeMap(DocumentWithVersionsDTO.class, Document.class)
              .addMappings(m -> m.skip(Document::setDocumentVersions)).setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(DocumentWithVersionsDTO source, Document destination) {
        if (!Objects.isNull(source.getDocumentVersionsIds())) {
            destination.setDocumentVersions(new HashSet<>(documentVersionRepository.findAllById(source.getDocumentVersionsIds())));
        } else {
            destination.setDocumentVersions(Collections.emptySet());
        }
    }

    @Override
    protected void mapSpecificFields(Document source, DocumentWithVersionsDTO destination) {
        destination.setDocumentVersionsIds(getIds(source));
    }

    @Override
    protected Set<Long> getIds(Document entity) {
        return Objects.isNull(entity) || Objects.isNull(entity.getId())
               ? null
               : entity.getDocumentVersions().stream()
                     .map(GenericModel::getId)
                     .collect(Collectors.toSet());
    }

}
