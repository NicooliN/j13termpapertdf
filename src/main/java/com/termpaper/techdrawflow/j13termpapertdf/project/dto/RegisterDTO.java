package com.termpaper.techdrawflow.j13termpapertdf.project.dto;

import lombok.Getter;
import lombok.Setter;
import java.time.LocalDate;


@Getter
@Setter
public class RegisterDTO
            extends UserDTO{

    private String login;

    private String password;

    private String firstName;

    private String lastName;

    private String middleName;

    private LocalDate birthDate;

    private String phone;

    private String email;

    private String position;

    private RoleDTO role;

    private Long departmentId;

}
