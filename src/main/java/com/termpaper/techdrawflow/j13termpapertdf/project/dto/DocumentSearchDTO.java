package com.termpaper.techdrawflow.j13termpapertdf.project.dto;

import com.termpaper.techdrawflow.j13termpapertdf.project.model.DocumentType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.Set;

@Getter
@Setter
@ToString

public class DocumentSearchDTO  {

    private String documentNumber;

    private String documentTitle;

    private DocumentType documentType;

}
