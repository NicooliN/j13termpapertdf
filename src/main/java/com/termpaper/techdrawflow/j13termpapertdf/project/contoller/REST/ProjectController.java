package com.termpaper.techdrawflow.j13termpapertdf.project.contoller.REST;

import com.termpaper.techdrawflow.j13termpapertdf.project.dto.ProjectDTO;
import com.termpaper.techdrawflow.j13termpapertdf.project.model.Project;
import com.termpaper.techdrawflow.j13termpapertdf.project.service.ProjectService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/projects")
@SecurityRequirement(name = "Bearer Authentication")
@CrossOrigin(origins = "*", allowedHeaders = "*")
@Tag(name = "Проекты", description = "контроллер для работы с проектами")
public class ProjectController
            extends GenericController<Project, ProjectDTO> {

    public ProjectController(ProjectService projectService) {
        super(projectService);
    }
}
