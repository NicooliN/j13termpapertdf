package com.termpaper.techdrawflow.j13termpapertdf.project.dto;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DocumentWithVersionsDTO
      extends DocumentDTO {

    private Set<DocumentVersionDTO> documentVersions;
}
