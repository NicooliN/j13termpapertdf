package com.termpaper.techdrawflow.j13termpapertdf.project.dto;

import lombok.*;

import java.time.LocalDate;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class UserDTO
        extends GenericDTO {

    private String login;
    private String password;
    private String firstName;
    private String lastName;
    private String middleName;
    private LocalDate birthDate;
    private String phone;
    private String email;
    private String position;
    private Long departmentId;
    private Long taskId;
    private RoleDTO role;
    private String changePasswordToken;
    //TODO ZDES 4TOTO neTAk
    private Set<Long> userTask;
    private boolean isDeleted;
}