package com.termpaper.techdrawflow.j13termpapertdf.project.model;

public enum DocumentType {


    DRAWINGS("КД"),
    TECHNOLOGY("ТД"),
    TEXT("Документ");

    private final String documentType;

    DocumentType(String documentType) {
        this.documentType = documentType;
    }
public String getDocumentType() {return  this.documentType;}
}
