package com.termpaper.techdrawflow.j13termpapertdf.project.service;

import com.termpaper.techdrawflow.j13termpapertdf.project.dto.TaskDTO;
import com.termpaper.techdrawflow.j13termpapertdf.project.mapper.TaskMapper;
import com.termpaper.techdrawflow.j13termpapertdf.project.model.Task;
import com.termpaper.techdrawflow.j13termpapertdf.project.repository.TaskRepository;
import org.springframework.stereotype.Service;

@Service
public class TaskService
        extends GenericService<Task, TaskDTO> {

    public TaskService(TaskRepository taskRepository, TaskMapper taskMapper) {
        super(taskRepository, taskMapper);
    }


}
