//package com.termpaper.techdrawflow.j13termpapertdf.project.contoller.REST;
//
//import com.termpaper.techdrawflow.j13termpapertdf.project.config.JWTSecurity.JWTTokenUtil;
//import com.termpaper.techdrawflow.j13termpapertdf.project.dto.LoginDTO;
//import com.termpaper.techdrawflow.j13termpapertdf.project.dto.UserDTO;
//import com.termpaper.techdrawflow.j13termpapertdf.project.model.User;
//import com.termpaper.techdrawflow.j13termpapertdf.project.service.UserService;
//import com.termpaper.techdrawflow.j13termpapertdf.project.service.userdetails.CustomUserDetailsService;
//import io.swagger.v3.oas.annotations.security.SecurityRequirement;
//import io.swagger.v3.oas.annotations.tags.Tag;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.security.access.prepost.PreAuthorize;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.web.bind.annotation.*;
//
//import java.util.HashMap;
//import java.util.Map;
//
//
//@RestController
//@SecurityRequirement(name = "Bearer Authentication")
//@CrossOrigin(origins = "*", maxAge = 3600)
//@Slf4j
//@RequestMapping( value = "/users")
//@Tag(name = "Пользователь", description = "Работа с пользователями")
//public class UserController extends GenericController<User, UserDTO> {
//
//private final CustomUserDetailsService customUserDetailsService;
//
//private final JWTTokenUtil jwtTokenUtil;
//
//private final UserService userService;
//    public UserController(UserService userService,
//                          CustomUserDetailsService customUserDetailsService,
//                          JWTTokenUtil jwtTokenUtil) {
//        super(userService);
//        this.customUserDetailsService = customUserDetailsService;
//        this.jwtTokenUtil = jwtTokenUtil;
//        this.userService = userService;
//    }
//
//    @PostMapping("/auth")
//    public ResponseEntity<?> auth(@RequestBody LoginDTO loginDTO) {
//        Map<String, Object> response = new HashMap<>();
//        log.info("LoginDTO: {}", loginDTO);
//        UserDetails foundUser = customUserDetailsService.loadUserByUsername(loginDTO.getLogin());
//        log.info("LoginDTO: {}", loginDTO);
//        if(!userService.checkPassword(loginDTO.getPassword(), foundUser)) {
//            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("ОШИБКА АВТОРИТИЗАЦИИ! Пароль не подходит!");
//        }
//        String token = jwtTokenUtil.generateToken(foundUser);
//        response.put("token", token);
//        response.put("username", foundUser.getUsername());
//        response.put("authorities", foundUser.getAuthorities());
//        return ResponseEntity.ok().body(response);
//    }
////
////    @PostMapping("/reg")
////    public ResponseEntity<?> reg(@RequestBody UserDTO userDTO) {
////        return ResponseEntity.status(HttpStatus.CREATED).body(userService.create(userDTO));
////    }
////
////    @PostMapping("/logout")
////    public ResponseEntity<?> logout(@RequestBody LoginDTO loginDTO) {
////        Map<String, Object> response = new HashMap<>();
////        response.put("token", null);
////        response.put("username", null);
////        response.put("authorities", null);
////        return ResponseEntity.ok().body(response);
////    }
////
////    @GetMapping("/getUser")
////    public ResponseEntity<?> getUser(@RequestBody LoginDTO loginDTO) {
////        return ResponseEntity.ok().body(userService.getUserByLogin(loginDTO.getLogin()));
////    }
////
////    @GetMapping("/all")
////    public String allAccess() {
////        return "Public Content.";
////    }
////
////    @GetMapping("/user")
////    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
////    public String userAccess() {
////        return "User Content.";
////    }
////
////    @GetMapping("/mod")
////    @PreAuthorize("hasRole('MODERATOR')")
////    public String moderatorAccess() {
////        return "Moderator Board.";
////    }
////
////    @GetMapping("/admin")
////    @PreAuthorize("hasRole('ADMIN')")
////    public String adminAccess() {
////        return "Admin Board.";
////    }
////
////    @Operation(description = "Вывести все фильмы юзера ", method = "getAllOrders")
////    @RequestMapping(value = "/getAllOrders", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
////    public ResponseEntity<Set<FilmDTO>> getAllOrders(@RequestParam(value = "userId") Long userId) {
////        return ResponseEntity.status(HttpStatus.OK).body(userService.getAllOrders(userId));
////    }
//
//}
