package com.termpaper.techdrawflow.j13termpapertdf.project.mapper;


import com.termpaper.techdrawflow.j13termpapertdf.project.dto.DocumentVersionDTO;
import com.termpaper.techdrawflow.j13termpapertdf.project.model.DocumentVersion;
import com.termpaper.techdrawflow.j13termpapertdf.project.repository.DocumentRepository;
import com.termpaper.techdrawflow.j13termpapertdf.project.repository.DocumentVersionRepository;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import org.webjars.NotFoundException;
import java.util.Set;
@Component
public class DocumentVersionMapper
        extends GenericMapper<DocumentVersion, DocumentVersionDTO> {


    private final DocumentRepository documentRepository;

    protected DocumentVersionMapper(ModelMapper mapper,
                                    DocumentVersionRepository documentVersionRepository,
                                    DocumentRepository documentRepository) {
        super(mapper, DocumentVersion.class, DocumentVersionDTO.class);

        this.documentRepository = documentRepository;

    }
    @PostConstruct
    @Override
    protected void setupMapper() {
        super.modelMapper.createTypeMap(DocumentVersion.class, DocumentVersionDTO.class)
                .addMappings(m -> m.skip(DocumentVersionDTO::setDocumentId)).setPostConverter(toDTOConverter());
//                .addMappings(m -> m.skip(DocumentVersionDTO::setDocumentVersionOwner)).setPostConverter(toDTOConverter());
        super.modelMapper.createTypeMap(DocumentVersionDTO.class, DocumentVersion.class)
                .addMappings(m -> m.skip(DocumentVersion::setDocument)).setPostConverter(toEntityConverter());

    }

    @Override
    protected void mapSpecificFields(DocumentVersionDTO source, DocumentVersion destination) {
        destination.setDocument(documentRepository.findById(source.getDocumentId()).orElseThrow(() -> new NotFoundException("data project not found by this id in DVM"))); //


    }
    @Override
    protected void mapSpecificFields(DocumentVersion source, DocumentVersionDTO destination) {
        destination.setDocumentId(source.getDocument().getId());
    }


    @Override
    protected Set<Long> getIds(DocumentVersion entity) {
        throw new UnsupportedOperationException("Метод недоступен");
    }




}
