package com.termpaper.techdrawflow.j13termpapertdf.project.constants;

public interface UserRolesConstants {
    String ADMIN = "ADMIN";
    String MODERATOR = "MODERATOR";
    String USER = "USER";
}
