package com.termpaper.techdrawflow.j13termpapertdf.project.mapper;


import com.termpaper.techdrawflow.j13termpapertdf.project.dto.ProjectWithDocumentsDTO;
import com.termpaper.techdrawflow.j13termpapertdf.project.model.GenericModel;
import com.termpaper.techdrawflow.j13termpapertdf.project.model.Project;
import com.termpaper.techdrawflow.j13termpapertdf.project.repository.DocumentRepository;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;


//мапер
@Component
public class ProjectWithDocumentsMapper
      extends GenericMapper<Project, ProjectWithDocumentsDTO> {

//    private final ProjectRepository projectRepository;
    private final DocumentRepository documentRepository;

    protected ProjectWithDocumentsMapper(ModelMapper mapper,
                                         DocumentRepository documentRepository) {
        super(mapper, Project.class, ProjectWithDocumentsDTO.class);
//        this.projectRepository = projectRepository;
        this.documentRepository = documentRepository;
    }



    @Override
    @PostConstruct
    protected void setupMapper() {
        modelMapper.createTypeMap(Project.class, ProjectWithDocumentsDTO.class)
              .addMappings(m -> m.skip(ProjectWithDocumentsDTO::setDocumentsIds)).setPostConverter(toDTOConverter());

        modelMapper.createTypeMap(ProjectWithDocumentsDTO.class, Project.class)
              .addMappings(m -> m.skip(Project::setDocuments)).setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(ProjectWithDocumentsDTO source, Project destination) {
        if (!Objects.isNull(source.getDocumentsIds())) {
            destination.setDocuments(new HashSet<>(documentRepository.findAllById(source.getDocumentsIds())));
        } else {
            destination.setDocuments(Collections.emptySet());
        }
    }

    @Override
    protected void mapSpecificFields(Project source, ProjectWithDocumentsDTO destination) {
        destination.setDocumentsIds(getIds(source));
    }

    @Override
    protected Set<Long> getIds(Project entity) {
        return Objects.isNull(entity) || Objects.isNull(entity.getId())
               ? null
               : entity.getDocuments().stream()
                     .map(GenericModel::getId)
                     .collect(Collectors.toSet());
    }
}
