package com.termpaper.techdrawflow.j13termpapertdf.project.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Set;

@Entity
@Getter @Setter @NoArgsConstructor

@SequenceGenerator(name = "default_generator", sequenceName = "tasks_seq", allocationSize = 1)
public class Task extends GenericModel{

@Id
private Long id;

@OneToOne(fetch = FetchType.LAZY)
@MapsId
    private DocumentVersion documentVersion;

@Column(name = "document")
    private String document;


    @Column(name = "period_signature")
    private LocalDate periodSignature;

@OneToMany(mappedBy = "task", cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    private Set<User> users_signature;

}
