package com.termpaper.techdrawflow.j13termpapertdf.project.mapper;

import com.termpaper.techdrawflow.j13termpapertdf.project.dto.TaskDTO;
import com.termpaper.techdrawflow.j13termpapertdf.project.model.Task;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public class TaskMapper
        extends GenericMapper<Task, TaskDTO> {

    public TaskMapper(ModelMapper modelMapper) {
        super(modelMapper, Task.class, TaskDTO.class);
    }

    @Override
    protected void mapSpecificFields(Task source, TaskDTO destination) {

    }

    @Override
    protected void mapSpecificFields(TaskDTO source, Task destination) {

    }

    @Override
    protected Set<Long> getIds(Task entity) {
        return null;
    }

    @Override
    protected void setupMapper() {

    }
}
