package com.termpaper.techdrawflow.j13termpapertdf.project.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "document_versions")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SequenceGenerator(name = "default_generator", sequenceName = "document_versions_seq", allocationSize = 1)
public class DocumentVersion extends GenericModel{


    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn (name = "document_id", foreignKey = @ForeignKey(name = "FK_DOCUMENT_VERSION_DOCUMENTS"))
    private Document document;

    @Column (name = "document_version_number", nullable = false)
    private Integer documentVersionNumber;

    @Column (name = "document_version_owner")
    private String documentVersionOwner;

    @Column(name = "description")
    private String description;

    @Column(name = "online_copy_path")
    private String onlineCopyPath;

}