package com.termpaper.techdrawflow.j13termpapertdf.project.contoller.MVC;

import com.termpaper.techdrawflow.j13termpapertdf.project.contoller.REST.GenericController;
import com.termpaper.techdrawflow.j13termpapertdf.project.dto.TaskDTO;
import com.termpaper.techdrawflow.j13termpapertdf.project.model.Task;
import com.termpaper.techdrawflow.j13termpapertdf.project.service.TaskService;
import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Controller
@Hidden
@RequestMapping("/tasks")
@Tag(name = "Задание", description = "")
public class MVCTaskController {

    private final TaskService taskService;
    public MVCTaskController(TaskService taskService) {
      this.taskService = taskService;
    }
}
