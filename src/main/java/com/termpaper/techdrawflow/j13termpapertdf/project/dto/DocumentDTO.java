package com.termpaper.techdrawflow.j13termpapertdf.project.dto;

import com.termpaper.techdrawflow.j13termpapertdf.project.model.DocumentType;
import lombok.*;

import java.time.LocalDate;
import java.util.Set;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class DocumentDTO extends GenericDTO {

    private String documentNumber;

    private String documentTitle;

    private DocumentType documentType;

    private Long userId;

    private Long projectId;

    private Set<Long> documentVersionsIds;

}
