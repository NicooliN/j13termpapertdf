package com.termpaper.techdrawflow.j13termpapertdf.project.repository;

import com.termpaper.techdrawflow.j13termpapertdf.project.model.DocumentVersion;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface DocumentVersionRepository
        extends GenericRepository<DocumentVersion> {



}
