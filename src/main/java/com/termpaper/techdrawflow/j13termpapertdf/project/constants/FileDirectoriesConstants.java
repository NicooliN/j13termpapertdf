package com.termpaper.techdrawflow.j13termpapertdf.project.constants;

public interface FileDirectoriesConstants {
    String DOCUMENT_VERSIONS_UPLOAD_DIRECTORY = "files/documentVersions";
}
