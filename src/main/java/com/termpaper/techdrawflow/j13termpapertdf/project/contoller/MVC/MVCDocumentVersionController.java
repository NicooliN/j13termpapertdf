package com.termpaper.techdrawflow.j13termpapertdf.project.contoller.MVC;



import com.termpaper.techdrawflow.j13termpapertdf.project.dto.DocumentVersionDTO;
import com.termpaper.techdrawflow.j13termpapertdf.project.exception.MyDeleteException;
import com.termpaper.techdrawflow.j13termpapertdf.project.service.DocumentService;
import com.termpaper.techdrawflow.j13termpapertdf.project.service.DocumentVersionService;
import io.swagger.v3.oas.annotations.Hidden;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Controller
@Hidden
@Slf4j
@RequestMapping("/documentVersions")
public class MVCDocumentVersionController {

    private final DocumentVersionService documentVersionService;

    private final DocumentService documentService;
    public MVCDocumentVersionController(DocumentVersionService documentVersionService,
                                        DocumentService documentService) {
      this.documentVersionService = documentVersionService;
      this.documentService = documentService;
    }

    @GetMapping("/list")
    public String listAllDocumentVersions(@RequestParam(value = "page", defaultValue = "1") int page,
                                   @RequestParam(value = "size", defaultValue = "10") int pageSize,
                                   @ModelAttribute(value = "exception") String exception,
                                   Model model) {
        log.info("Hello from log");
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Direction.ASC, "document_version_number"));
        Page<DocumentVersionDTO> documentVersionsPage = documentVersionService.listAll(pageRequest);
        model.addAttribute("documentVersions", documentVersionsPage);
        model.addAttribute("exception", exception);
        return "documentVersions/viewDocumentVersions";
    }

    @GetMapping("/{id}")
    public String getOne(@PathVariable Long id,
                         Model model) {
        model.addAttribute("documentVersions", documentVersionService.getOne(id));
        return "documentVersions/viewDocumentVersions";
    }

    @GetMapping("/add/{documentId}")
    public String create(@PathVariable Long documentId,
                         Model model) {
        model.addAttribute("documentId", documentId);
        model.addAttribute("document" , documentService.getOne(documentId).getDocumentNumber());
        return "documentVersions/addDocumentVersions";
    }

    @PostMapping("/add")
    public String create(@ModelAttribute("documentVersionForm")
                             DocumentVersionDTO documentVersionDTO,
                         @RequestParam MultipartFile file) {
        if (file != null && file.getSize() > 0) {
            documentVersionService.create(documentVersionDTO, file);
        }
        else {
            documentVersionService.create(documentVersionDTO);
        }
        log.info(documentVersionDTO.toString());
        return "redirect:/documents/" + documentVersionDTO.getDocumentId().toString();
    }


    @GetMapping(value = "/download", produces = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ResponseBody
    public ResponseEntity<Resource> downloadDocumentsVersions(@Param(value = "documentVersionId") Long documentVersionId) throws IOException {
        DocumentVersionDTO documentVersionDTO = documentVersionService.getOne(documentVersionId);
        Path path = Paths.get(documentVersionDTO.getOnlineCopyPath());
        ByteArrayResource resource = new ByteArrayResource(Files.readAllBytes(path));

        return ResponseEntity.ok()
                .headers(this.headers(path.getFileName().toString()))
                .contentLength(path.toFile().length())
                .contentType(MediaType.parseMediaType("application/octet-stream"))
                .body(resource);
    }

    private HttpHeaders headers(String name) {
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + name);
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");
        return headers;
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable Long id) throws MyDeleteException {
        String documentId = documentVersionService.getOne(id).getDocumentId().toString();
        documentVersionService.delete(id);
        return "redirect:/documents/" + documentId;
    }

    @GetMapping("/restore/{id}")
    public String restore(@PathVariable Long id) {
        String documentId = documentVersionService.getOne(id).getDocumentId().toString();
        documentVersionService.restore(id);
        return "redirect:/documents/" + documentId;
    }

}
