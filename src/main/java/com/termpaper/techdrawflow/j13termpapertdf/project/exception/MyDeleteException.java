package com.termpaper.techdrawflow.j13termpapertdf.project.exception;

public class MyDeleteException
      extends Exception {
    public MyDeleteException(String message) {
        super(message);
    }
}
