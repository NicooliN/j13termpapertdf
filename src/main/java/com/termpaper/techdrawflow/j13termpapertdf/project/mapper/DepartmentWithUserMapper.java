package com.termpaper.techdrawflow.j13termpapertdf.project.mapper;

import com.termpaper.techdrawflow.j13termpapertdf.project.dto.DepartmentDTO;
import com.termpaper.techdrawflow.j13termpapertdf.project.dto.DepartmentWithUserDTO;
import com.termpaper.techdrawflow.j13termpapertdf.project.model.Department;
import com.termpaper.techdrawflow.j13termpapertdf.project.model.GenericModel;
import com.termpaper.techdrawflow.j13termpapertdf.project.repository.UserRepository;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class DepartmentWithUserMapper
        extends GenericMapper<Department, DepartmentWithUserDTO>  {


    private final UserRepository userRepository;
    public DepartmentWithUserMapper(ModelMapper modelMapper,
                                    UserRepository userRepository) {
        super(modelMapper, Department.class, DepartmentWithUserDTO.class);
        this.userRepository = userRepository;
    }


    @Override
    @PostConstruct
    protected void setupMapper() {
        modelMapper.createTypeMap(Department.class, DepartmentWithUserDTO.class)
                .addMappings(m -> m.skip(DepartmentWithUserDTO::setUsersIds)).setPostConverter(toDTOConverter());
        modelMapper.createTypeMap(DepartmentWithUserDTO.class, Department.class)
                .addMappings(m -> m.skip(Department::setUsers)).setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(DepartmentWithUserDTO source, Department destination) {
        if (!Objects.isNull(source.getUsersIds())) {
            destination.setUsers(new HashSet<>(userRepository.findAllById(source.getUsersIds())));
        }
        else {
            destination.setUsers(Collections.emptySet());
        }
    }

    @Override
    protected void mapSpecificFields(Department source, DepartmentWithUserDTO destination) {
        destination.setUsersIds(getIds(source));
    }

    @Override
    protected Set<Long> getIds(Department entity) {
        return Objects.isNull(entity) || Objects.isNull(entity.getId())
                ? null
                : entity.getUsers().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toSet());
    }

}
