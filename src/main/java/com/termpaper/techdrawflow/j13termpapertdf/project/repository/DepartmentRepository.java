package com.termpaper.techdrawflow.j13termpapertdf.project.repository;


import com.termpaper.techdrawflow.j13termpapertdf.project.model.Department;
import org.springframework.stereotype.Repository;
@Repository
public interface DepartmentRepository
        extends GenericRepository<Department>{
}
