package com.termpaper.techdrawflow.j13termpapertdf.project.dto;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DocumentVersionDTO extends GenericDTO {

    private Long documentId;

    private Integer documentVersionNumber;

    private String documentVersionOwner;

    private String description;

    private String onlineCopyPath;
}
