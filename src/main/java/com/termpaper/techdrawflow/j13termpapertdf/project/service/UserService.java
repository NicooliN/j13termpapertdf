package com.termpaper.techdrawflow.j13termpapertdf.project.service;


import com.termpaper.techdrawflow.j13termpapertdf.project.constants.MailConstants;
import com.termpaper.techdrawflow.j13termpapertdf.project.dto.RoleDTO;
import com.termpaper.techdrawflow.j13termpapertdf.project.dto.UserDTO;
import com.termpaper.techdrawflow.j13termpapertdf.project.mapper.UserMapper;
import com.termpaper.techdrawflow.j13termpapertdf.project.model.User;
import com.termpaper.techdrawflow.j13termpapertdf.project.repository.UserRepository;
import com.termpaper.techdrawflow.j13termpapertdf.project.util.MailUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

import java.time.LocalDateTime;

import java.util.List;
import java.util.UUID;

import static com.termpaper.techdrawflow.j13termpapertdf.project.constants.UserRolesConstants.ADMIN;

@Slf4j
@Service
public class UserService
        extends GenericService<User, UserDTO> {


    private final JavaMailSender javaMailSender;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    protected UserService(UserRepository userRepository,
                          UserMapper userMapper,
                          BCryptPasswordEncoder bCryptPasswordEncoder,
                          JavaMailSender javaMailSender) {
        super(userRepository, userMapper);
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
      this.javaMailSender = javaMailSender;
    }

    public Page<UserDTO> findUsers(UserDTO userDTO,
                                   Pageable pageable) {
        Page<User> users = ((UserRepository) repository).searchUsers(userDTO.getFirstName(),
                userDTO.getLastName(),
                userDTO.getLogin(),
                pageable);
        List<UserDTO> result = mapper.toDTOs(users.getContent());
        return new PageImpl<>(result, pageable, users.getTotalElements());
    }

    public UserDTO getUserByLogin(final String login) {
        return mapper.toDTO(((UserRepository) repository).findUserByLogin(login));
    }

    public UserDTO getUserByEmail(final String email) {
        return mapper.toDTO(((UserRepository) repository).findUserByEmail(email));
    }

    public Boolean checkPassword(String password, UserDetails userDetails) {
        return bCryptPasswordEncoder.matches(password, userDetails.getPassword());
    }

    //TODO
//    public List<String> getUserEmailsWithDelayedRentDate() {
//        return ((UserRepository) repository).getDelayedEmails();
//    }

    @Override
    public UserDTO create(UserDTO object) {
        RoleDTO roleDTO = new RoleDTO();
        String userName = SecurityContextHolder.getContext().getAuthentication().getName();
        if (ADMIN.equalsIgnoreCase(userName)) {
            roleDTO.setId(2L);//библиотекарь
        }
        else {
            roleDTO.setId(1L);//пользователь
        }
    object.setRole(roleDTO);
    object.setCreatedBy("REGISTRATION FORM");
    object.setCreatedWhen(LocalDateTime.now());
    object.setLogin(object.getLogin());
    object.setPassword(bCryptPasswordEncoder.encode(object.getPassword()));
    object.setFirstName(object.getFirstName());
    object.setLastName(object.getLastName());
    object.setMiddleName(object.getMiddleName());
    object.setEmail(object.getEmail());
    object.setPhone(object.getPhone());
    object.setBirthDate(object.getBirthDate());
    object.setDepartmentId(object.getDepartmentId());
    log.info(object.toString());
    return mapper.toDTO(repository.save(mapper.toEntity(object)));
    }
    public void sendChangePasswordEmail(final UserDTO userDTO) {
        UUID uuid = UUID.randomUUID();
        userDTO.setChangePasswordToken(uuid.toString());
        update(userDTO);
        SimpleMailMessage mailMessage = MailUtil.createEmailMessage(userDTO.getEmail(),
                MailConstants.MAIL_SUBJECT_FOR_REMEMBER_PASSWORD,
                MailConstants.MAIL_MESSAGE_FOR_REMEMBER_PASSWORD + uuid);
        javaMailSender.send(mailMessage);
    }


    public void changePassword(final String uuid,
                               final String password) {
        UserDTO user = mapper.toDTO(((UserRepository) repository).findUserByChangePasswordToken(uuid));
        user.setChangePasswordToken(null);
        user.setPassword(bCryptPasswordEncoder.encode(password));
        update(user);
    }

    public void restore(Long objectId) {
        User user = repository.findById(objectId).orElseThrow(
                () -> new NotFoundException("Пользователя с заданным ID=" + objectId + " не существует"));
        unMarkAsDeleted(user);
        repository.save(user);
    }

//    public Set<FilmDTO> getAllOrders(final Long userId) {
//        //get user
//        User user = repository.findById(directorId).orElseThrow(() -> new NotFoundException("Director not find")));
//        Set<Order> orders = user.getUserOrders();//get all users order
//        Set<FilmDTO> filmsDTO = new HashSet<>();
//        for (Order order : orders){
//           filmsDTO.add(userMapper.toDTO(order.getFilm()));
//        }
//       return filmsDTO; //get film from all order
//    }

    // private Set<Long> userOrders;
}
