package com.termpaper.techdrawflow.j13termpapertdf.project.util;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DateFormatter {
    private DateFormatter() {
    }


    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    /***
     * Метод форматирующий поданную на вход строку в нужный формат даты и возвращающий дату.
     * @param dateToFormat - Строка, которую нужно преобразовать в дату.
     * @return - LocalDate: дата в нужном формате.
     */
    public static LocalDate formatStringToDate(final String dateToFormat) {
        return LocalDate.parse(dateToFormat, FORMATTER);
    }

}
