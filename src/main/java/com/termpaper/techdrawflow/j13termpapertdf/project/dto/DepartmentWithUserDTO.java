package com.termpaper.techdrawflow.j13termpapertdf.project.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class DepartmentWithUserDTO
        extends DepartmentDTO{

    Set<UserDTO> users;
}
