package com.termpaper.techdrawflow.j13termpapertdf.project.contoller.REST;


import com.termpaper.techdrawflow.j13termpapertdf.project.model.Document;
import com.termpaper.techdrawflow.j13termpapertdf.project.service.DocumentService;
import io.swagger.v3.oas.annotations.tags.Tag;
import com.termpaper.techdrawflow.j13termpapertdf.project.dto.DocumentDTO;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;



@RestController
@RequestMapping("/documents")
@Tag(name = "Документация", description = "Контроллер для работы с документацией")
public class DocumentController extends GenericController<Document,  DocumentDTO> {

    private final DocumentService documentService;

    public DocumentController(DocumentService documentService) {
        super(documentService);
        this.documentService = documentService;
    }

//    @Operation(description = "Добавить режисера к фильму", method = "addDirectorToFilm")
//    @RequestMapping(value = "/addDirectorToFilm", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<FilmDTO> addDirectorToFilm(@RequestParam(value = "filmId") Long filmId, @RequestParam(value = "directorId") Long directorId) {
//        return ResponseEntity.status(HttpStatus.CREATED).body(filmService.addDirectorToFilm(filmId, directorId));
//    }
}
