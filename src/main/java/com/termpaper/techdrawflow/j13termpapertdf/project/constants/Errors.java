package com.termpaper.techdrawflow.j13termpapertdf.project.constants;


public interface Errors {
    class Document {
        public static final String DOCUMENT_DELETE_ERROR = "Документ не может быть удалена";
    }

    class DocumentVersion{
        public static final String DOCUMENT_VERSION_DELETE_ERROR = "Извещение не может быть удалено";
    }

    class Users{
        public static final String USER_FORBIDDEN_ERROR = "У вас нет прав просматривать информацию о пользователе";
    }
}
