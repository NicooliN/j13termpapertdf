package com.termpaper.techdrawflow.j13termpapertdf.project.contoller.MVC;

import com.termpaper.techdrawflow.j13termpapertdf.project.contoller.REST.GenericController;
import com.termpaper.techdrawflow.j13termpapertdf.project.dto.DocumentSearchDTO;
import com.termpaper.techdrawflow.j13termpapertdf.project.dto.ProjectDTO;
import com.termpaper.techdrawflow.j13termpapertdf.project.dto.UserDTO;
import com.termpaper.techdrawflow.j13termpapertdf.project.exception.MyDeleteException;
import com.termpaper.techdrawflow.j13termpapertdf.project.model.Project;
import com.termpaper.techdrawflow.j13termpapertdf.project.service.DocumentService;
import com.termpaper.techdrawflow.j13termpapertdf.project.service.ProjectService;
import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import java.util.List;
@Slf4j
@Controller
@Hidden
@RequestMapping("/projects")
public class MVCProjectController {

    private final ProjectService projectService;
    private final DocumentService documentService;
    public MVCProjectController(ProjectService projectService,
                                DocumentService documentService
    ) {
        this.projectService = projectService;
        this.documentService = documentService;
    }

    @GetMapping("/list")
    public String listAllProjects( @ModelAttribute(name = "exception") final String exception,
                                   Model model) {
        List<ProjectDTO> result = projectService.listAll();
        model.addAttribute("projects", result);
        model.addAttribute("exception", exception);
        return "projects/viewAllProjects";}


//Здесь контроллер на взятие проекта
    @GetMapping("/{id}")
    public String getProject(@PathVariable Long id,
                             Model model) {
        model.addAttribute("project", projectService.getProject(id));
        return "projects/viewProject";
    }

    @PostMapping("/{id}/search")
    public String searchDocuments(@RequestParam(value = "page", defaultValue = "1") int page,
                                  @RequestParam(value = "size", defaultValue = "5") int size,
                                  @ModelAttribute("documentSearchForm") DocumentSearchDTO documentSearchDTO,
                                  @PathVariable Long id,
                                  Model model) {
        PageRequest pageRequest = PageRequest.of(page - 1, size, Sort.by(Sort.Direction.ASC, "document_number"));
        model.addAttribute("documents", documentService.findDocuments(documentSearchDTO, pageRequest));
        model.addAttribute("project", projectService.getProject(id));
        return "projects/viewProject";
    }

    @GetMapping("/add")
    public String create() {
//        model.addAttribute("projectForm", new ProjectDTO());
        return "projects/addProject";}

    @PostMapping("/add")
    public String create(@ModelAttribute("projectForm") ProjectDTO projectDTO) {
        projectService.create(projectDTO);
        return "redirect:/projects/list";
    }

    @PostMapping("/search")
    public String searchProject(@ModelAttribute("projectSearchForm") ProjectDTO projectDTO, Model model) {
        List<ProjectDTO> result = projectService.findProject(projectDTO);
        model.addAttribute("projects", result);
        return "projects/viewAllProjects";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable Long id) throws MyDeleteException {
        projectService.delete(id);
        return "redirect:/projects/list";
    }

    @GetMapping("/restore/{id}")
    public String restore(@PathVariable Long id) {
        projectService.restore(id);
        return "redirect:/projects/list";
    }
    @ExceptionHandler(MyDeleteException.class)
    public RedirectView handleError(HttpServletRequest req,
                                    Exception ex,
                                    RedirectAttributes redirectAttributes) {
        log.error("Запрос: " + req.getRequestURL() + " вызвал ошибку " + ex.getMessage());
        redirectAttributes.addFlashAttribute("exception", ex.getMessage());
        return new RedirectView("/projects", true);
    }

    @GetMapping("/update/{id}")
    public String update(@PathVariable Long id,
                         Model model) {
        model.addAttribute("project", projectService.getOne(id));
        return "projects/updateProject";
    }

    @PostMapping("/update")
    public String update(@ModelAttribute("projectForm") ProjectDTO projectDTO) {
        projectService.update(projectDTO);
        return "redirect:/projects/list";
    }
}
