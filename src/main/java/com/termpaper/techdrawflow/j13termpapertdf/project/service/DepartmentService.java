package com.termpaper.techdrawflow.j13termpapertdf.project.service;

import com.termpaper.techdrawflow.j13termpapertdf.project.dto.DepartmentDTO;
import com.termpaper.techdrawflow.j13termpapertdf.project.dto.DepartmentWithUserDTO;
import com.termpaper.techdrawflow.j13termpapertdf.project.dto.DocumentWithVersionsDTO;
import com.termpaper.techdrawflow.j13termpapertdf.project.dto.ProjectWithDocumentsDTO;
import com.termpaper.techdrawflow.j13termpapertdf.project.exception.MyDeleteException;
import com.termpaper.techdrawflow.j13termpapertdf.project.mapper.DepartmentMapper;
import com.termpaper.techdrawflow.j13termpapertdf.project.mapper.DepartmentWithUserMapper;
import com.termpaper.techdrawflow.j13termpapertdf.project.mapper.GenericMapper;
import com.termpaper.techdrawflow.j13termpapertdf.project.model.Department;
import com.termpaper.techdrawflow.j13termpapertdf.project.model.Document;
import com.termpaper.techdrawflow.j13termpapertdf.project.model.Project;
import com.termpaper.techdrawflow.j13termpapertdf.project.repository.DepartmentRepository;
import com.termpaper.techdrawflow.j13termpapertdf.project.repository.GenericRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

import java.util.List;

@Service
public class DepartmentService
        extends GenericService<Department, DepartmentDTO>{

    private final DepartmentWithUserMapper departmentWithUserMapper;

    private final DepartmentRepository departmentRepository;

    public DepartmentService(DepartmentRepository departmentRepository,
                             DepartmentMapper departmentMapper,
                             DepartmentWithUserMapper departmentWithUserMapper) {
        super(departmentRepository, departmentMapper);
        this.departmentWithUserMapper = departmentWithUserMapper;
        this.departmentRepository = departmentRepository;
    }

    public DepartmentWithUserDTO getDepartment(Long id) {
        return departmentWithUserMapper.toDTO(mapper.toEntity(super.getOne(id)));
    }

    public void restore(Long objectId) {
        Department department = departmentRepository.findById(objectId).orElseThrow(
                () -> new NotFoundException("Подразделение с заданным ID=" + objectId + " не существует"));
        unMarkAsDeleted(department);
        repository.save(department);
    }

    @Override
    public void delete(Long id) throws MyDeleteException {
        Department department = departmentRepository.findById(id).orElseThrow(
                () -> new NotFoundException("Подразделение с заданным ID=" + id + " не существует"));
        markAsDeleted(department);
        departmentRepository.save(department);
    }

}
