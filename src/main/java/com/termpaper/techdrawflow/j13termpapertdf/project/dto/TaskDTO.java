package com.termpaper.techdrawflow.j13termpapertdf.project.dto;

import com.termpaper.techdrawflow.j13termpapertdf.project.model.GenericModel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import java.time.LocalDate;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
    public class TaskDTO extends GenericDTO {

        private Long id;

        private Long documentVersionId;

        private String document;

        private LocalDate periodSignature;

        private Set<Long> users_signature;
}
