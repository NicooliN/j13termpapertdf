package com.termpaper.techdrawflow.j13termpapertdf.project.contoller.MVC;


import com.termpaper.techdrawflow.j13termpapertdf.project.dto.DepartmentDTO;
import com.termpaper.techdrawflow.j13termpapertdf.project.exception.MyDeleteException;
import com.termpaper.techdrawflow.j13termpapertdf.project.service.DepartmentService;
import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import java.util.List;



@Slf4j
@Controller
@Hidden
@RequestMapping("/departments")
@Tag(name = "Подразделения", description = "контроллер для работы с подразделениями")
public class MVCDepartmentController {

 private final DepartmentService departmentService;
    public MVCDepartmentController(DepartmentService departmentService){
        this.departmentService = departmentService;
    }

    @GetMapping("/list")
    public String listAllProjects(Model model) {
        List<DepartmentDTO> result = departmentService.listAll();
        model.addAttribute("departments", result);
        return "departments/viewAllDepartments";}

//    @GetMapping("/{id}")
//    public String  getOne(@PathVariable Long id,
//                          Model model) {
//        model.addAttribute("department", departmentService.getDepartment(id));
//        return "departments/viewAllDepartments";
//    }

    @GetMapping("/add")
    public String create() {
        return "departments/addDepartment";}

    @PostMapping("/add")
    public String create(@ModelAttribute("departmentForm") DepartmentDTO departmentDTO) {
        departmentService.create(departmentDTO);
        log.info(departmentDTO.toString());
        return "redirect:/departments/list";
    }

    @GetMapping("/update/{id}")
    public String update(@PathVariable Long id,
                         Model model) {
        model.addAttribute("department", departmentService.getOne(id));
        return "departments/updateDepartment";
    }

    @PostMapping("/update")
    public String update(@ModelAttribute("departmentForm") DepartmentDTO departmentDTO) {
        departmentService.update(departmentDTO);
        return "redirect:/departments/list";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable Long id) throws MyDeleteException {
        departmentService.delete(id);
        return "redirect:/departments";
    }

    @GetMapping("/restore/{id}")
    public String restore(@PathVariable Long id) {
        departmentService.restore(id);
        return "redirect:/departments";
    }
    @ExceptionHandler(MyDeleteException.class)
    public RedirectView handleError(HttpServletRequest req,
                                    Exception ex,
                                    RedirectAttributes redirectAttributes) {
        log.error("Запрос: " + req.getRequestURL() + " вызвал ошибку " + ex.getMessage());
        redirectAttributes.addFlashAttribute("exception", ex.getMessage());
        return new RedirectView("/departments", true);
    }
}
