package com.termpaper.techdrawflow.j13termpapertdf.project.service;

import com.termpaper.techdrawflow.j13termpapertdf.project.dto.GenericDTO;
import com.termpaper.techdrawflow.j13termpapertdf.project.exception.MyDeleteException;
import com.termpaper.techdrawflow.j13termpapertdf.project.mapper.GenericMapper;
import com.termpaper.techdrawflow.j13termpapertdf.project.model.GenericModel;
import com.termpaper.techdrawflow.j13termpapertdf.project.repository.GenericRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;


import java.time.LocalDateTime;
import java.util.List;

@Service
@Slf4j
public abstract class GenericService<T extends GenericModel, D extends GenericDTO> {

    protected final GenericRepository<T> repository;

    protected final GenericMapper<T, D> mapper;

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    protected GenericService(GenericRepository<T> repository,
                             GenericMapper<T, D> mapper){
        this.repository = repository;
        this.mapper = mapper;
    }

    //TODO listAll for Pageable Service

    public List<D> listAll(){
        return mapper.toDTOs(repository.findAll());
    }

    public Page<D> listAll(Pageable pageable) {
        Page<T> objects = repository.findAll(pageable);
        List<D> result = mapper.toDTOs(objects.getContent());
        return new PageImpl<>(result, pageable, objects.getTotalElements());
    }

    public D getOne(final Long id) {
        return mapper.toDTO(repository.findById(id).orElseThrow(() -> new NotFoundException("data not found by this id")));
    }

    public D create(D newObject) {
        log.info("In service input####" + newObject.toString());
        newObject.setCreatedBy(SecurityContextHolder.getContext().getAuthentication().getName());
        newObject.setCreatedWhen(LocalDateTime.now());
        return mapper.toDTO(repository.save(mapper.toEntity(newObject)));
    }

    public D update(D newObject) {
        newObject.setCreatedBy(SecurityContextHolder.getContext().getAuthentication().getName());
        newObject.setCreatedWhen(LocalDateTime.now());
        return mapper.toDTO(repository.save(mapper.toEntity(newObject)));
    }

    public void delete(final Long id) throws MyDeleteException {
        repository.deleteById(id);
    }

    public void markAsDeleted(GenericModel genericModel) {
        genericModel.setDeleted(true);
        genericModel.setDeletedWhen(LocalDateTime.now());
        genericModel.setDeletedBy(SecurityContextHolder.getContext().getAuthentication().getName());
    }

    public void unMarkAsDeleted(GenericModel genericModel) {
        genericModel.setDeleted(false);
        genericModel.setDeletedWhen(null);
        genericModel.setDeletedBy(null);
    }

}
