package com.termpaper.techdrawflow.j13termpapertdf.project.service;

import com.termpaper.techdrawflow.j13termpapertdf.project.constants.Errors;
import com.termpaper.techdrawflow.j13termpapertdf.project.dto.DocumentDTO;
import com.termpaper.techdrawflow.j13termpapertdf.project.dto.DocumentVersionDTO;
import com.termpaper.techdrawflow.j13termpapertdf.project.dto.DocumentWithVersionsDTO;
import com.termpaper.techdrawflow.j13termpapertdf.project.exception.MyDeleteException;
import com.termpaper.techdrawflow.j13termpapertdf.project.mapper.DocumentVersionMapper;
import com.termpaper.techdrawflow.j13termpapertdf.project.model.Document;
import  com.termpaper.techdrawflow.j13termpapertdf.project.model.DocumentVersion;
import com.termpaper.techdrawflow.j13termpapertdf.project.repository.DocumentVersionRepository;
import com.termpaper.techdrawflow.j13termpapertdf.project.util.FileManager;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.webjars.NotFoundException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class DocumentVersionService
                    extends GenericService<DocumentVersion, DocumentVersionDTO> {

    private final DocumentVersionRepository documentVersionRepository;
    protected DocumentVersionService(DocumentVersionRepository documentVersionRepository,
                                     DocumentVersionMapper documentVersionMapper) {
        super(documentVersionRepository, documentVersionMapper);
        this.documentVersionRepository = documentVersionRepository;
    }


    public DocumentVersionDTO create(final DocumentVersionDTO object,
                          MultipartFile file) {
        String fileName = FileManager.createFile(file);
        object.setOnlineCopyPath(fileName);
        object.setCreatedBy(SecurityContextHolder.getContext().getAuthentication().getName());
        object.setCreatedWhen(LocalDateTime.now());
        return mapper.toDTO(documentVersionRepository.save(mapper.toEntity(object)));
    }



//    public Page<DocumentDTO> findDocuments(DocumentDTO documentDTO, Pageable pageable) {
//        Page<Document> documents = documentRepository.searchDocuments(documentDTO.getDocumentNumber(),
//                documentDTO.getDocumentTitle(),
//                documentDTO.getDocumentType(),
//                pageable);
//        List<DocumentDTO> result = mapper.toDTOs(documents.getContent());
//        return new PageImpl<>(result, pageable, documents.getTotalElements());
//    }
//
    public void restore(Long objectId) {
        DocumentVersion documentVersion = documentVersionRepository.findById(objectId).orElseThrow(
                () -> new NotFoundException("Изменение документа с заданным ID=" + objectId + " не существует"));
        unMarkAsDeleted(documentVersion);
        repository.save(documentVersion);
    }
    @Override
    public void delete(Long id) throws MyDeleteException {
        DocumentVersion documentVersion = documentVersionRepository.findById(id).orElseThrow(
                () -> new NotFoundException("Изменение с заданным ID=" + id + " не существует"));
//        boolean bookCanBeDeleted = repository.findBookByIdAndBookRentInfosReturnedFalseAndIsDeletedFalse(id) == null;
            markAsDeleted(documentVersion);
        documentVersionRepository.save(documentVersion);
        }

}
