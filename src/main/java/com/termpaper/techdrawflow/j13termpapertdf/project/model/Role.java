package com.termpaper.techdrawflow.j13termpapertdf.project.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "roles")
@Getter
@Setter
@NoArgsConstructor
@SequenceGenerator(name = "default_generator", sequenceName = "role_seq",allocationSize = 1)

public class Role{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;
    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "description", nullable = false)
    private String description;
}


//CREATE ROLE IN PsqL
////    insert into roles
////values (1, 'Роль пользователя', 'USER', now(),'paul'),
////        (2, 'Роль модератора', 'MODERATOR',now(),'paul');