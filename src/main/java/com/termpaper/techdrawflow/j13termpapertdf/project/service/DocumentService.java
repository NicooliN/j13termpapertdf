package com.termpaper.techdrawflow.j13termpapertdf.project.service;


import com.termpaper.techdrawflow.j13termpapertdf.project.constants.Errors;
import com.termpaper.techdrawflow.j13termpapertdf.project.dto.*;
import com.termpaper.techdrawflow.j13termpapertdf.project.exception.MyDeleteException;
import com.termpaper.techdrawflow.j13termpapertdf.project.mapper.DocumentMapper;
import com.termpaper.techdrawflow.j13termpapertdf.project.mapper.DocumentWithVersionsMapper;
import com.termpaper.techdrawflow.j13termpapertdf.project.model.Document;
import com.termpaper.techdrawflow.j13termpapertdf.project.model.DocumentVersion;
import com.termpaper.techdrawflow.j13termpapertdf.project.model.User;
import com.termpaper.techdrawflow.j13termpapertdf.project.repository.DocumentRepository;
import com.termpaper.techdrawflow.j13termpapertdf.project.repository.UserRepository;
import com.termpaper.techdrawflow.j13termpapertdf.project.util.FileManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

import java.util.List;

@Slf4j
@Service
public class DocumentService
        extends GenericService<Document, DocumentDTO>{

    private final DocumentRepository documentRepository;

    private final DocumentWithVersionsMapper documentWithVersionsMapper;

    protected DocumentService(DocumentRepository documentRepository,
                              DocumentMapper documentMapper,
                              DocumentWithVersionsMapper documentWithVersionsMapper) {
        super(documentRepository, documentMapper);
        this.documentRepository = documentRepository;
        this.documentWithVersionsMapper = documentWithVersionsMapper;
    }

    public Page<DocumentWithVersionsDTO> getAllDocumentsWithVersions(Pageable pageable) {

        Page<Document> documentsPaginated = documentRepository.findAll(pageable);
        log.info(documentsPaginated.toString());
        List<DocumentWithVersionsDTO> result = documentWithVersionsMapper.toDTOs(documentsPaginated.getContent());
        return new PageImpl<>(result, pageable, documentsPaginated.getTotalElements());
    }

    public Page<DocumentWithVersionsDTO> findDocuments(DocumentSearchDTO documentSearchDTO,
                                                        Pageable pageable) {
        String documentType = documentSearchDTO.getDocumentType() != null ? String.valueOf(documentSearchDTO.getDocumentType().ordinal()) : null;
        Page<Document> documentsPaginated  = documentRepository.searchDocuments(documentSearchDTO.getDocumentNumber(),
                documentSearchDTO.getDocumentTitle(),
                documentType,
                pageable);
        List<DocumentWithVersionsDTO> result = documentWithVersionsMapper.toDTOs(documentsPaginated.getContent());
        log.info(documentsPaginated.stream().toList().toString());
        return new PageImpl<>(result, pageable, documentsPaginated.getTotalElements());
    }

    public void restore(Long objectId) {
        Document document = documentRepository.findById(objectId).orElseThrow(
                () -> new NotFoundException("Документа с заданным ID=" + objectId + " не существует"));
        unMarkAsDeleted(document);
        documentRepository.save(document);
    }

    public DocumentWithVersionsDTO getDocument(Long id) {
        return documentWithVersionsMapper.toDTO(mapper.toEntity(super.getOne(id)));
    }

    @Override
    public void delete(Long id) throws MyDeleteException {
        Document document = documentRepository.findById(id).orElseThrow(
                () -> new NotFoundException("Изменение с заданным ID=" + id + " не существует"));
            markAsDeleted(document);
            documentRepository.save(document);

    }
}
