package com.termpaper.techdrawflow.j13termpapertdf.project.repository;

import com.termpaper.techdrawflow.j13termpapertdf.project.model.Task;
import org.springframework.stereotype.Repository;

@Repository
public interface TaskRepository
        extends GenericRepository<Task>{
}
