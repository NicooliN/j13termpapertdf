package com.termpaper.techdrawflow.j13termpapertdf.project.contoller.REST;



import com.termpaper.techdrawflow.j13termpapertdf.project.dto.GenericDTO;
import com.termpaper.techdrawflow.j13termpapertdf.project.exception.MyDeleteException;
import com.termpaper.techdrawflow.j13termpapertdf.project.model.GenericModel;
import com.termpaper.techdrawflow.j13termpapertdf.project.service.GenericService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@CrossOrigin(origins = "http://localhost:8080/", allowedHeaders = "*")
@RestController
public abstract class GenericController <T extends GenericModel, D extends GenericDTO> {

    private final GenericService<T, D> genericService;


    public GenericController(GenericService genericService){
        this.genericService = genericService;
    }
    @Operation(description = "Получить все записи", method = "getAll")
    @RequestMapping(value = "/getAll", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<D>> getAll (){
        return ResponseEntity.status(HttpStatus.OK).body(genericService.listAll());
    }

    @Operation(description = "Получить информацию по ID", method = "getById")
    @RequestMapping(value = "/getById", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<D> getById (@RequestParam(value = "id") Long id){
        return ResponseEntity.status(HttpStatus.OK).body(genericService.getOne(id));
    }

    @Operation(description = "Создать", method = "create")
    @RequestMapping(value = "/create", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<D> create (@RequestBody D createEntity) {
        createEntity.setCreatedWhen(LocalDateTime.now());
        return ResponseEntity.status(HttpStatus.CREATED).body(genericService.create(createEntity));
    }

    @Operation(description = "Обновить", method = "update")
    @RequestMapping(value = "/update", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<D> update (@RequestBody D updateEntity,
                                     @RequestParam(value = "id") Long id){
        updateEntity.setId(id);
        return ResponseEntity.status(HttpStatus.CREATED).body(genericService.update(updateEntity));
    }

    @Operation(description = "Удалить", method = "delete")
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    void delete (@RequestParam(value = "id") Long id) throws MyDeleteException {
        genericService.delete(id);
    }

}


