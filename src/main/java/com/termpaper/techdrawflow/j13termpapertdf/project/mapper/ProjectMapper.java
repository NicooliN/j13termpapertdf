package com.termpaper.techdrawflow.j13termpapertdf.project.mapper;

import com.termpaper.techdrawflow.j13termpapertdf.project.dto.ProjectDTO;
import com.termpaper.techdrawflow.j13termpapertdf.project.model.GenericModel;
import com.termpaper.techdrawflow.j13termpapertdf.project.model.Project;
import com.termpaper.techdrawflow.j13termpapertdf.project.repository.DocumentRepository;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class ProjectMapper
        extends GenericMapper<Project, ProjectDTO>{

    private final DocumentRepository documentRepository;
    public ProjectMapper(ModelMapper modelMapper,
                         DocumentRepository documentRepository) {
        super(modelMapper, Project.class, ProjectDTO.class);
        this.documentRepository = documentRepository;
    }
    @Override
    @PostConstruct
    protected void setupMapper() {
        modelMapper.createTypeMap(Project.class, ProjectDTO.class)
              .addMappings(m -> m.skip(ProjectDTO::setDocumentsIds)).setPostConverter(toDTOConverter());

        modelMapper.createTypeMap(ProjectDTO.class, Project.class)
              .addMappings(m -> m.skip(Project::setDocuments)).setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(ProjectDTO source, Project destination) {
        if (!Objects.isNull(source.getDocumentsIds())) {
        destination.setDocuments(new HashSet<>(documentRepository.findAllById(source.getDocumentsIds())));
    }
        else {
        destination.setDocuments(Collections.emptySet());
    }

}

    @Override
    protected void mapSpecificFields(Project source, ProjectDTO destination) {
        destination.setDocumentsIds(getIds(source));
    }

    @Override
    protected Set<Long> getIds(Project entity) {
        return Objects.isNull(entity) || Objects.isNull(entity.getId())
               ? null
               : entity.getDocuments().stream()
                     .map(GenericModel::getId)
                     .collect(Collectors.toSet());
    }

}
