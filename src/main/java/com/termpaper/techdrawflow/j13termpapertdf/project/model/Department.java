package com.termpaper.techdrawflow.j13termpapertdf.project.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Entity
@AllArgsConstructor
@Table(name = "departments")
@Getter @Setter @NoArgsConstructor
@SequenceGenerator(name =" default_generator", sequenceName = "departments_seq", allocationSize = 1)
public class Department extends GenericModel{

    @Column(name = "name_department")
    private String nameDepartment;

    @Column(name = "number_department")
    private String numberDepartment;

    @OneToMany(mappedBy = "department", cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    private Set<User> users;
}