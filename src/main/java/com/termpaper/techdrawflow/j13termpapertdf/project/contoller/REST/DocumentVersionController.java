package com.termpaper.techdrawflow.j13termpapertdf.project.contoller.REST;

import com.termpaper.techdrawflow.j13termpapertdf.project.dto.DocumentVersionDTO;
import com.termpaper.techdrawflow.j13termpapertdf.project.model.DocumentVersion;
import com.termpaper.techdrawflow.j13termpapertdf.project.service.DocumentVersionService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/documentsVersions")
@Tag(name = "Версия документов", description = "Контроллер для работы с версиями документов")
public class DocumentVersionController
        extends GenericController <DocumentVersion, DocumentVersionDTO>{
    public DocumentVersionController(DocumentVersionService documentVersionService) {
        super(documentVersionService);
    }
}
