package com.termpaper.techdrawflow.j13termpapertdf.project.mapper;

import com.termpaper.techdrawflow.j13termpapertdf.project.dto.DocumentDTO;
import com.termpaper.techdrawflow.j13termpapertdf.project.model.Document;
import com.termpaper.techdrawflow.j13termpapertdf.project.model.GenericModel;
import com.termpaper.techdrawflow.j13termpapertdf.project.repository.DocumentRepository;
import com.termpaper.techdrawflow.j13termpapertdf.project.repository.DocumentVersionRepository;
import com.termpaper.techdrawflow.j13termpapertdf.project.repository.ProjectRepository;
import com.termpaper.techdrawflow.j13termpapertdf.project.repository.UserRepository;
import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import org.webjars.NotFoundException;

import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
@Slf4j
@Component
public class DocumentMapper
        extends GenericMapper<Document, DocumentDTO> {

    private final DocumentVersionRepository documentVersionRepository;
    private final ProjectRepository projectRepository;

    private final UserRepository userRepository;

    protected DocumentMapper(ModelMapper mapper,
                             DocumentVersionRepository documentVersionRepository,
                             DocumentRepository documentRepository,
                             ProjectRepository projectRepository,
                             UserRepository userRepository) {
        super(mapper, Document.class, DocumentDTO.class);

        this.documentVersionRepository = documentVersionRepository;
        this.projectRepository = projectRepository;
        this.userRepository = userRepository;
    }



    @PostConstruct
    protected void setupMapper() {
        modelMapper.createTypeMap(Document.class, DocumentDTO.class)
                .addMappings(m -> m.skip(DocumentDTO::setDocumentVersionsIds)).setPostConverter(toDTOConverter())
                .addMappings(m -> m.skip(DocumentDTO::setProjectId)).setPostConverter(toDTOConverter())
                .addMappings(m -> m.skip(DocumentDTO::setUserId)).setPostConverter(toDTOConverter());
        modelMapper.createTypeMap(DocumentDTO.class, Document.class)
                .addMappings(m -> m.skip(Document::setDocumentVersions)).setPostConverter(toEntityConverter())
                 .addMappings(m -> m.skip(Document::setProject)).setPostConverter(toEntityConverter())
                .addMappings(m -> m.skip(Document::setUser)).setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(DocumentDTO source, Document destination) {
        if (!Objects.isNull(source.getDocumentVersionsIds())) {
            destination.setDocumentVersions(new HashSet<>(documentVersionRepository.findAllById(source.getDocumentVersionsIds())));
        } else {
            destination.setDocumentVersions(Collections.emptySet());
        }
        destination.setProject(projectRepository.findById(source.getProjectId()).orElseThrow(() -> new NotFoundException("data project not found by this id in DM"))); //

        destination.setUser(userRepository.findById(source.getUserId()).orElseThrow(() -> new NotFoundException("data user not found by this id in DM"))); //

    }
    @Override
    protected void mapSpecificFields(Document source, DocumentDTO destination) {
        destination.setDocumentVersionsIds(getIds(source));
        destination.setProjectId(source.getProject().getId());
        destination.setUserId(source.getUser().getId());
    }



    @Override
    protected Set<Long> getIds(Document entity) {
   return Objects.isNull(entity) || Objects.isNull(entity.getId())
            ? null
            : entity.getDocumentVersions().stream()
                     .map(GenericModel::getId)
                     .collect(Collectors.toSet());
            }

}
