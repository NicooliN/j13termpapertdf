package com.termpaper.techdrawflow.j13termpapertdf.project.service;

import com.termpaper.techdrawflow.j13termpapertdf.project.dto.ProjectDTO;
import com.termpaper.techdrawflow.j13termpapertdf.project.model.Project;


import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public interface ProjectTestData {

    ProjectDTO PROJECT_DTO_1 = new ProjectDTO("projectNumber01",
            "projectNumber01",
            new HashSet<>());

    ProjectDTO PROJECT_DTO_2 = new ProjectDTO("projectNumber02",
            "projectNumber02",
            new HashSet<>());

    ProjectDTO PROJECT_DTO_3  = new ProjectDTO("projectNumber03",
            "projectNumber03",
            new HashSet<>());

    List<ProjectDTO> PROJECT_DTO_LIST = Arrays.asList(PROJECT_DTO_1, PROJECT_DTO_2, PROJECT_DTO_3);


    Project PROJECT_1  = new Project(null,
            "projectNumber01",
            "projectNumber01");

    Project PROJECT_2  = new Project(null,
            "projectNumber02",
            "projectNumber02");
    Project PROJECT_3  = new Project(null,
            "projectNumber03",
            "projectNumber03");

    List<Project> PROJECT_LIST = Arrays.asList(PROJECT_1, PROJECT_2, PROJECT_3);
}
