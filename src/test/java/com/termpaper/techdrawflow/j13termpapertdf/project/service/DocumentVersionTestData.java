package com.termpaper.techdrawflow.j13termpapertdf.project.service;


import com.termpaper.techdrawflow.j13termpapertdf.project.dto.DocumentVersionDTO;
import com.termpaper.techdrawflow.j13termpapertdf.project.model.DocumentVersion;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public interface DocumentVersionTestData {

    DocumentVersionDTO DOCUMENT_VERSION_DTO_1 = new DocumentVersionDTO(1L,
            1,
           "documentVersionNumber01",
            "documentVersionNumber01",
            "documentVersionNumber01");

    DocumentVersionDTO DOCUMENT_VERSION_DTO_2 = new DocumentVersionDTO(2L,
            2,
            "documentVersionNumber02",
            "documentVersionNumber02",
            "documentVersionNumber02");

    DocumentVersionDTO DOCUMENT_VERSION_DTO_3 = new DocumentVersionDTO(3L,
            3,
            "documentVersionNumber03",
            "documentVersionNumber03",
            "documentVersionNumber03");

    List<DocumentVersionDTO> DOCUMENT_VERSION_DTO_LIST = Arrays.asList(DOCUMENT_VERSION_DTO_1, DOCUMENT_VERSION_DTO_2, DOCUMENT_VERSION_DTO_3);


    DocumentVersion DOCUMENT_VERSION_1 = new DocumentVersion(null,
            1,
            "documentVersionNumber01",
            "documentVersionNumber01",
            "documentVersionNumber01");

    DocumentVersion DOCUMENT_VERSION_2 = new DocumentVersion(null,
            2,
            "documentVersionNumber02",
            "documentVersionNumber02",
            "documentVersionNumber02");

    DocumentVersion DOCUMENT_VERSION_3 = new DocumentVersion(null,
            3,
            "documentVersionNumber03",
            "documentVersionNumber03",
            "documentVersionNumber03");

    List<DocumentVersion> DOCUMENT_VERSION_LIST = Arrays.asList(DOCUMENT_VERSION_1, DOCUMENT_VERSION_2, DOCUMENT_VERSION_3);
}



//    private Long documentId;
//    private Integer documentVersionNumber;
//    private String documentVersionOwner;
//    private String description;
//    private String onlineCopyPath;
