package com.termpaper.techdrawflow.j13termpapertdf.project.service;

import com.termpaper.techdrawflow.j13termpapertdf.project.dto.DocumentDTO;
import com.termpaper.techdrawflow.j13termpapertdf.project.dto.ProjectDTO;
import com.termpaper.techdrawflow.j13termpapertdf.project.exception.MyDeleteException;
import com.termpaper.techdrawflow.j13termpapertdf.project.mapper.ProjectMapper;
import com.termpaper.techdrawflow.j13termpapertdf.project.mapper.ProjectWithDocumentsMapper;
import com.termpaper.techdrawflow.j13termpapertdf.project.model.Document;
import com.termpaper.techdrawflow.j13termpapertdf.project.model.Project;
import com.termpaper.techdrawflow.j13termpapertdf.project.repository.ProjectRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.Mockito;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@Slf4j
public class ProjectServiceTest extends GenericServiceTest<Project, ProjectDTO> {

    private final ProjectRepository projectRepository = Mockito.mock(ProjectRepository.class);

    private final ProjectMapper projectMapper = Mockito.mock(ProjectMapper.class);

    private final ProjectWithDocumentsMapper projectWithDocumentsMapper = Mockito.mock(ProjectWithDocumentsMapper.class);;
    public ProjectServiceTest() {
        super();
        service = new ProjectService(projectRepository, projectMapper, projectWithDocumentsMapper);
    }
    @Test
    @Order(1)
    @Override
    protected void getAll() {
        Mockito.when(projectRepository.findAll()).thenReturn(ProjectTestData.PROJECT_LIST);
        Mockito.when(projectMapper.toDTOs(ProjectTestData.PROJECT_LIST)).thenReturn(ProjectTestData.PROJECT_DTO_LIST);
        List<ProjectDTO> projectDTOS = service.listAll();
        System.out.println(projectDTOS);
        assertEquals(ProjectTestData.PROJECT_LIST.size(), projectDTOS.size());
    }
    @Test
    @Order(2)
    @Override
    protected void getOne() {
        Mockito.when(projectRepository.findById(1L)).thenReturn(Optional.of(ProjectTestData.PROJECT_1));
        Mockito.when(projectMapper.toDTO(ProjectTestData.PROJECT_1)).thenReturn(ProjectTestData.PROJECT_DTO_1);
        ProjectDTO projectDTO = service.getOne(1L);
        log.info("Testing getOne(): " + projectDTO);
        assertEquals(ProjectTestData.PROJECT_DTO_1, projectDTO);
    }


    @Test
    @Order(4)
    @Override
    protected void create() {
        Mockito.when(projectMapper.toEntity(ProjectTestData.PROJECT_DTO_1)).thenReturn(ProjectTestData.PROJECT_1);
        Mockito.when(projectMapper.toDTO(ProjectTestData.PROJECT_1)).thenReturn(ProjectTestData.PROJECT_DTO_1);
        Mockito.when(projectRepository.save(ProjectTestData.PROJECT_1)).thenReturn(ProjectTestData.PROJECT_1);
        ProjectDTO projectDTO = service.create(ProjectTestData.PROJECT_DTO_1);
        log.info("Testing create(): " + projectDTO);
        assertEquals(ProjectTestData.PROJECT_DTO_1, projectDTO);
    }
    @Test
    @Order(5)
    @Override
    protected void update() {
        Mockito.when(projectMapper.toEntity(ProjectTestData.PROJECT_DTO_1)).thenReturn(ProjectTestData.PROJECT_1);
        Mockito.when(projectMapper.toDTO(ProjectTestData.PROJECT_1)).thenReturn(ProjectTestData.PROJECT_DTO_1);
        Mockito.when(projectRepository.save(ProjectTestData.PROJECT_1)).thenReturn(ProjectTestData.PROJECT_1);
        ProjectDTO projectDTO = service.update(ProjectTestData.PROJECT_DTO_1);
        log.info("Testing update(): " + projectDTO);
        assertEquals(ProjectTestData.PROJECT_DTO_1, projectDTO);
    }
    @Test
    @Order(6)
    @Override
    protected void delete() throws MyDeleteException {

    }
    @Test
    @Order(7)
    @Override
    protected void restore() {

    }
    @Test
    @Order(8)
    @Override
    protected void getAllNotDeleted() {

    }
}
