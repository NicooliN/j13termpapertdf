package com.termpaper.techdrawflow.j13termpapertdf.project.service;

import com.termpaper.techdrawflow.j13termpapertdf.project.dto.DocumentDTO;
import com.termpaper.techdrawflow.j13termpapertdf.project.model.Document;
import com.termpaper.techdrawflow.j13termpapertdf.project.model.DocumentType;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;


public interface DocumentTestData {

    DocumentDTO DOCUMENT_DTO_1 = new DocumentDTO("documentNumber01",
            "documentTitle01",
            DocumentType.DRAWINGS ,
            1L,
            2L,
            new HashSet<>());

    DocumentDTO DOCUMENT_DTO_2 = new DocumentDTO("documentNumber02",
            "documentTitle02",
            DocumentType.DRAWINGS ,
            1L,
            2L,
            new HashSet<>());

    DocumentDTO DOCUMENT_DTO_3 = new DocumentDTO("documentNumber03",
            "documentTitle03",
            DocumentType.DRAWINGS ,
            2L,
            1L,
            new HashSet<>());

    List<DocumentDTO> DOCUMENT_DTO_LIST = Arrays.asList(DOCUMENT_DTO_1, DOCUMENT_DTO_2, DOCUMENT_DTO_3);


    Document DOCUMENT_1 = new Document("documentNumber01",
            "documentTitle01",
            DocumentType.DRAWINGS ,
            null,
            null,
            new HashSet<>());

    Document DOCUMENT_2 = new Document("documentNumber02",
            "documentTitle02",
            DocumentType.DRAWINGS ,
            null,
            null,
            new HashSet<>());

    Document DOCUMENT_3 = new Document("documentNumber03",
            "documentTitle03",
            DocumentType.DRAWINGS ,
            null,
            null,
            new HashSet<>());

    List<Document> DOCUMENT_LIST = Arrays.asList(DOCUMENT_1, DOCUMENT_2, DOCUMENT_3);
}



