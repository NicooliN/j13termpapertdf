package com.termpaper.techdrawflow.j13termpapertdf.project.service;


import com.termpaper.techdrawflow.j13termpapertdf.project.dto.DocumentVersionDTO;
import com.termpaper.techdrawflow.j13termpapertdf.project.exception.MyDeleteException;
import com.termpaper.techdrawflow.j13termpapertdf.project.mapper.DocumentVersionMapper;
import com.termpaper.techdrawflow.j13termpapertdf.project.model.DocumentVersion;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.Mockito;
import java.util.List;
import java.util.Optional;
import com.termpaper.techdrawflow.j13termpapertdf.project.repository.DocumentVersionRepository;
import static org.junit.jupiter.api.Assertions.assertEquals;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@Slf4j
public class DocumentVersionServiceTest extends GenericServiceTest<DocumentVersion, DocumentVersionDTO> {

    private final DocumentVersionRepository documentVersionRepository = Mockito.mock(DocumentVersionRepository.class);

    private final DocumentVersionMapper documentVersionMapper = Mockito.mock(DocumentVersionMapper.class);

    public DocumentVersionServiceTest() {
        super();
        service = new DocumentVersionService(documentVersionRepository, documentVersionMapper);
    }
    @Test
    @Order(1)
    @Override
    protected void getAll() {
        Mockito.when(documentVersionRepository.findAll()).thenReturn(DocumentVersionTestData.DOCUMENT_VERSION_LIST);
        Mockito.when(documentVersionMapper.toDTOs(DocumentVersionTestData.DOCUMENT_VERSION_LIST)).thenReturn(DocumentVersionTestData.DOCUMENT_VERSION_DTO_LIST);
        List<DocumentVersionDTO> documentVersionDTOS = service.listAll();
        System.out.println(documentVersionDTOS);
        assertEquals(DocumentVersionTestData.DOCUMENT_VERSION_LIST.size(), documentVersionDTOS.size());
    }
    @Test
    @Order(2)
    @Override
    protected void getOne() {
        Mockito.when(documentVersionRepository.findById(1L)).thenReturn(Optional.of(DocumentVersionTestData.DOCUMENT_VERSION_1));
        Mockito.when(documentVersionMapper.toDTO(DocumentVersionTestData.DOCUMENT_VERSION_1)).thenReturn(DocumentVersionTestData.DOCUMENT_VERSION_DTO_1);
        DocumentVersionDTO documentVersionDTO = service.getOne(1L);
        log.info("Testing getOne(): " + documentVersionDTO);
        assertEquals(DocumentVersionTestData.DOCUMENT_VERSION_DTO_1, documentVersionDTO);
    }


    @Test
    @Order(4)
    @Override
    protected void create() {
        Mockito.when(documentVersionMapper.toEntity(DocumentVersionTestData.DOCUMENT_VERSION_DTO_1)).thenReturn(DocumentVersionTestData.DOCUMENT_VERSION_1);
        Mockito.when(documentVersionMapper.toDTO(DocumentVersionTestData.DOCUMENT_VERSION_1)).thenReturn(DocumentVersionTestData.DOCUMENT_VERSION_DTO_1);
        Mockito.when(documentVersionRepository.save(DocumentVersionTestData.DOCUMENT_VERSION_1)).thenReturn(DocumentVersionTestData.DOCUMENT_VERSION_1);
        DocumentVersionDTO documentVersionDTO = service.create(DocumentVersionTestData.DOCUMENT_VERSION_DTO_1);
        log.info("Testing create(): " + documentVersionDTO);
        assertEquals(DocumentVersionTestData.DOCUMENT_VERSION_DTO_1, documentVersionDTO);
    }
    @Test
    @Order(5)
    @Override
    protected void update() {
        Mockito.when(documentVersionMapper.toEntity(DocumentVersionTestData.DOCUMENT_VERSION_DTO_1)).thenReturn(DocumentVersionTestData.DOCUMENT_VERSION_1);
        Mockito.when(documentVersionMapper.toDTO(DocumentVersionTestData.DOCUMENT_VERSION_1)).thenReturn(DocumentVersionTestData.DOCUMENT_VERSION_DTO_1);
        Mockito.when(documentVersionRepository.save(DocumentVersionTestData.DOCUMENT_VERSION_1)).thenReturn(DocumentVersionTestData.DOCUMENT_VERSION_1);
        DocumentVersionDTO documentVersionDTO = service.update(DocumentVersionTestData.DOCUMENT_VERSION_DTO_1);
        log.info("Testing update(): " + documentVersionDTO);
        assertEquals(DocumentVersionTestData.DOCUMENT_VERSION_DTO_1, documentVersionDTO);
    }
    @Test
    @Order(6)
    @Override
    protected void delete() throws MyDeleteException {

    }
    @Test
    @Order(7)
    @Override
    protected void restore() {

    }
    @Test
    @Order(8)
    @Override
    protected void getAllNotDeleted() {
    }

}

