package com.termpaper.techdrawflow.j13termpapertdf.project.service;

import com.termpaper.techdrawflow.j13termpapertdf.project.dto.DocumentDTO;
import com.termpaper.techdrawflow.j13termpapertdf.project.exception.MyDeleteException;
import com.termpaper.techdrawflow.j13termpapertdf.project.mapper.DocumentMapper;
import com.termpaper.techdrawflow.j13termpapertdf.project.mapper.DocumentWithVersionsMapper;
import com.termpaper.techdrawflow.j13termpapertdf.project.model.Document;
import com.termpaper.techdrawflow.j13termpapertdf.project.repository.DocumentRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.Mockito;

import java.util.List;
import java.util.Optional;
import static org.junit.jupiter.api.Assertions.assertEquals;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@Slf4j
public class DocumentServiceTest extends GenericServiceTest <Document, DocumentDTO> {

    private final DocumentRepository documentRepository = Mockito.mock(DocumentRepository.class);

    private final DocumentMapper documentMapper = Mockito.mock(DocumentMapper.class);

    private final DocumentWithVersionsMapper documentWithVersionsMapper = Mockito.mock(DocumentWithVersionsMapper.class);;
    public DocumentServiceTest() {
        super();
        service = new DocumentService(documentRepository, documentMapper, documentWithVersionsMapper);
    }
    @Test
    @Order(1)
    @Override
    protected void getAll() {
        Mockito.when(documentRepository.findAll()).thenReturn(DocumentTestData.DOCUMENT_LIST);
        Mockito.when(documentMapper.toDTOs(DocumentTestData.DOCUMENT_LIST)).thenReturn(DocumentTestData.DOCUMENT_DTO_LIST);
        List<DocumentDTO> documentDTOS = service.listAll();
        System.out.println(documentDTOS);
        assertEquals(DocumentTestData.DOCUMENT_LIST.size(), documentDTOS.size());
    }
    @Test
    @Order(2)
    @Override
    protected void getOne() {
        Mockito.when(documentRepository.findById(1L)).thenReturn(Optional.of(DocumentTestData.DOCUMENT_1));
        Mockito.when(documentMapper.toDTO(DocumentTestData.DOCUMENT_1)).thenReturn(DocumentTestData.DOCUMENT_DTO_1);
        DocumentDTO documentDTO = service.getOne(1L);
        log.info("Testing getOne(): " + documentDTO);
        assertEquals(DocumentTestData.DOCUMENT_DTO_1, documentDTO);
    }


    @Test
    @Order(4)
    @Override
    protected void create() {
        Mockito.when(documentMapper.toEntity(DocumentTestData.DOCUMENT_DTO_1)).thenReturn(DocumentTestData.DOCUMENT_1);
        Mockito.when(documentMapper.toDTO(DocumentTestData.DOCUMENT_1)).thenReturn(DocumentTestData.DOCUMENT_DTO_1);
        Mockito.when(documentRepository.save(DocumentTestData.DOCUMENT_1)).thenReturn(DocumentTestData.DOCUMENT_1);
        DocumentDTO documentDTO = service.create(DocumentTestData.DOCUMENT_DTO_1);
        log.info("Testing create(): " + documentDTO);
        assertEquals(DocumentTestData.DOCUMENT_DTO_1, documentDTO);
    }
    @Test
    @Order(5)
    @Override
    protected void update() {
        Mockito.when(documentMapper.toEntity(DocumentTestData.DOCUMENT_DTO_1)).thenReturn(DocumentTestData.DOCUMENT_1);
        Mockito.when(documentMapper.toDTO(DocumentTestData.DOCUMENT_1)).thenReturn(DocumentTestData.DOCUMENT_DTO_1);
        Mockito.when(documentRepository.save(DocumentTestData.DOCUMENT_1)).thenReturn(DocumentTestData.DOCUMENT_1);
        DocumentDTO documentDTO = service.update(DocumentTestData.DOCUMENT_DTO_1);
        log.info("Testing update(): " + documentDTO);
        assertEquals(DocumentTestData.DOCUMENT_DTO_1, documentDTO);
    }
    @Test
    @Order(6)
    @Override
    protected void delete() throws MyDeleteException {

    }
    @Test
    @Order(7)
    @Override
    protected void restore() {

    }
    @Test
    @Order(8)
    @Override
    protected void getAllNotDeleted() {

    }
}
