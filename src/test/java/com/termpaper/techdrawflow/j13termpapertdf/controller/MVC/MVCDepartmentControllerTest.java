package com.termpaper.techdrawflow.j13termpapertdf.controller.MVC;



import com.termpaper.techdrawflow.j13termpapertdf.project.dto.DepartmentDTO;
import com.termpaper.techdrawflow.j13termpapertdf.project.dto.ProjectDTO;
import com.termpaper.techdrawflow.j13termpapertdf.project.service.DepartmentService;
import com.termpaper.techdrawflow.j13termpapertdf.project.service.ProjectService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Slf4j
@Transactional
@Rollback(value = false)
public class MVCDepartmentControllerTest
      extends CommonTestMVC {
    
    //Создаем нового автора для создания через контроллер (тест дата)
    private final DepartmentDTO departmentDTO = new DepartmentDTO("departmentName_01", "description_01", new HashSet<>());
    private final DepartmentDTO departmentDTOUpdate = new DepartmentDTO("departmentName_01_UPDATE", "description_01_UPDATE", new HashSet<>());;
    
    @Autowired
    private DepartmentService departmentService;
    

    @Test
    @DisplayName("Просмотр всех подразделений через MVC контроллер, тестирование '/list'")
    @Order(0)
    @WithAnonymousUser
    @Override
    protected void listAll() throws Exception {
        log.info("Тест по выбору всех подрразделений через MVC начат");
        MvcResult result1 = mvc.perform(get("/departments/list")
//                                             .param("page", "1")
//                                             .param("size", "5")
                                             .contentType(MediaType.APPLICATION_JSON_VALUE)
                                             .accept(MediaType.APPLICATION_JSON_VALUE)
                                      )
              .andDo(print())
              .andExpect(status().is2xxSuccessful())
              .andExpect(view().name("departments/viewAllDepartments"))
              .andExpect(model().attributeExists("departments"))
              .andReturn();
    }
    

    @Test
    @DisplayName("Создание подразделения через MVC контроллер, тестирование 'departments/add'")
    @Order(1)
    @WithMockUser(username = "admin", roles = "ADMIN", password = "admin")
    @Override
    protected void createObject() throws Exception {
        log.info("Тест по созданию подразделения через MVC начат успешно");
        mvc.perform(post("/departments/add")
                          .contentType(MediaType.APPLICATION_JSON_VALUE)
                          .flashAttr("departmentForm", departmentDTO)
                          .accept(MediaType.APPLICATION_JSON_VALUE)
                          .with(csrf()))
              .andDo(print())
              .andExpect(status().is3xxRedirection())
              .andExpect(view().name("redirect:/departments/list"))
              .andExpect(redirectedUrlTemplate("/departments/list"))
              .andExpect(redirectedUrl("/departments/list"));
        log.info("Тест по созданию подразделения через MVC закончен успешно");
    }
    
    @Order(2)
    @Test
    @DisplayName("Обновление подразделения через MVC контроллер, тестирование 'departments/update'")
    @WithMockUser(username = "admin", roles = "ADMIN", password = "admin")
    //@WithUserDetails(userDetailsServiceBeanName = "customUserDetailsService", value = "andy_user")
    @Override
    protected void updateObject() throws Exception {
        log.info("Тест по обновлению подразделения через MVC начат успешно");
        departmentDTO.setNameDepartment(departmentDTOUpdate.getNameDepartment());
        mvc.perform(post("/departments/update")
                          .contentType(MediaType.APPLICATION_JSON_VALUE)
                          .flashAttr("departmentForm", departmentDTO)
                          .accept(MediaType.APPLICATION_JSON_VALUE)
                   )
              .andDo(print())
              .andExpect(status().is3xxRedirection())
              .andExpect(view().name("redirect:/departments/list"))
              .andExpect(redirectedUrl("/departments/list"));
        log.info("Тест по обновлению подразделения через MVC закончен успешно");
    }

}
