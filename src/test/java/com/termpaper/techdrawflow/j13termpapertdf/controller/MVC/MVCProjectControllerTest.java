package com.termpaper.techdrawflow.j13termpapertdf.controller.MVC;



import com.termpaper.techdrawflow.j13termpapertdf.project.dto.ProjectDTO;
import com.termpaper.techdrawflow.j13termpapertdf.project.service.ProjectService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Slf4j
@Transactional
@Rollback(value = false)
public class MVCProjectControllerTest
      extends CommonTestMVC {
    
    //Создаем нового автора для создания через контроллер (тест дата)
    private final ProjectDTO projectDTO = new ProjectDTO("projectName_01", "description_01", new HashSet<>());
    private final ProjectDTO projectDTOUpdate = new ProjectDTO("projectName_01_UPDATE", "description_01_UPDATE", new HashSet<>());;
    
    @Autowired
    private ProjectService projectService;
    

    @Test
    @DisplayName("Просмотр всех проектов через MVC контроллер, тестирование 'project/list'")
    @Order(0)
    @WithAnonymousUser
    @Override
    protected void listAll() throws Exception {
        log.info("Тест по выбору всех проектов через MVC начат");
        MvcResult result = mvc.perform(get("/projects/list")
//                                             .param("page", "1")
//                                             .param("size", "5")
                                             .contentType(MediaType.APPLICATION_JSON_VALUE)
                                             .accept(MediaType.APPLICATION_JSON_VALUE)
                                      )
              .andDo(print())
              .andExpect(status().is2xxSuccessful())
              .andExpect(view().name("projects/viewAllProjects"))
              .andExpect(model().attributeExists("projects"))
              .andReturn();
    }
    
    /**
     * Метод, тестирующий создание автора через MVC-контроллер.
     * Авторизуемся под пользователем admin (можно выбрать любого),
     * создаем шаблон данных и вызываем MVC-контроллер с соответствующим маппингом и методом.
     * flashAttr - используется, чтобы передать ModelAttribute в метод контроллера
     * Ожидаем, что будет статус redirect (как у нас в контроллере) при успешном создании
     *
     * @throws Exception - любая ошибка
     */
    @Test
    @DisplayName("Создание проектa через MVC контроллер, тестирование 'project/add'")
    @Order(1)
    @WithMockUser(username = "admin", roles = "ADMIN", password = "admin")
    @Override
    protected void createObject() throws Exception {
        log.info("Тест по созданию проектa через MVC начат успешно");
        mvc.perform(post("/projects/add")
                          .contentType(MediaType.APPLICATION_JSON_VALUE)
                          .flashAttr("projectForm", projectDTO)
                          .accept(MediaType.APPLICATION_JSON_VALUE)
                          .with(csrf()))
              .andDo(print())
              .andExpect(status().is3xxRedirection())
              .andExpect(view().name("redirect:/projects/list"))
              .andExpect(redirectedUrlTemplate("/projects/list"))
              .andExpect(redirectedUrl("/projects/list"));
        log.info("Тест по созданию проектa через MVC закончен успешно");
    }
    
    @Order(2)
    @Test
    @DisplayName("Обновление проектa через MVC контроллер, тестирование 'projects/update'")
    @WithMockUser(username = "admin", roles = "ADMIN", password = "admin")
    //@WithUserDetails(userDetailsServiceBeanName = "customUserDetailsService", value = "paul")
    @Override
    protected void updateObject() throws Exception {
        log.info("Тест по обновлению проектa через MVC начат успешно");
        projectDTO.setProjectName(projectDTOUpdate.getProjectName());
        mvc.perform(post("/projects/update")
                          .contentType(MediaType.APPLICATION_JSON_VALUE)
                          .flashAttr("projectForm", projectDTO)
                          .accept(MediaType.APPLICATION_JSON_VALUE)
                   )
              .andDo(print())
              .andExpect(status().is3xxRedirection())
              .andExpect(view().name("redirect:/projects/list"))
              .andExpect(redirectedUrl("/projects/list"));
        log.info("Тест по обновлению проектa через MVC закончен успешно");
    }

}
